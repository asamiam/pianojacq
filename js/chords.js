// 
// Chords table, built up from information 
// on Wikipedia:
// https://en.wikipedia.org/wiki/List_of_chords;
// verified using several public sources.
// 
// Note that chords nomenclature is messy, that
// not everybody agrees on what to name a certain
// chord and that there are plenty of 'enharmonic' 
// chords, different ways to spell the same sound
// depending on the context of the piece.
//
// Each chord is listed from the 'root' note going 
// up, the 'pitch classes' are numbered 1 through
// 11 and onwards to ensure 1:1 correspondence between
// pitches and notes played (otherwise some chords
// might contain repeated notes when in fact they
// should have been played an octave higher). If it
// is desirable to show the pitch class from 0...11 
// instead then simply modulo with 12 and show 10 and
// 11 as 't' and 'e' respectively.
// 
// To complicate matters chords can have multiple names,
// and the various abbreviations used are not always
// consistent (+ for augmented, - for flat, # for sharp
// for instance), and there are multiple symbols in 
// use to denote the same thing '♭', 'b', 'flat' and '-' all
// denote a flat.
//
// This table should be checked thoroughly by someone
// with a lot more knowledge about music theory than me,
// and expanded or modified until it is as close to perfect
// as we can get it. A lot depends on the quality of this
// table. 
//
// We should add the most likely fingerings for the 
// chords, possibly not just for the piano, but for
// instance also for the guitar. 
//

var chords = [

        // two tone chords (some would argue these are not true chords)

        { nshort: "3",     nlong: "third",   names: ["third"],                                  pitches: [ 0, 4] },
        { nshort: "5",     nlong: "fifth",   names: ["fifth", "power"],                         pitches: [ 0, 7] },
        { nshort: "8",     nlong: "octave",  names: ["octave"],                                 pitches: [ 0, 12] },

        // common triads

        { nshort: "sus2",  nlong: "sus2",    names: ["Sus2", "2"],                              pitches: [ 0, 2, 7] },
        { nshort: "o",     nlong: "dim",     names: ["Diminished", "Diminished triad", "m♭5", "MI(♭5)"],  
                                                                                                pitches: [ 0, 3, 6] },
        { nshort: "m",     nlong: "min",     names: ["Minor triad"],                            pitches: [ 0, 3, 7 ] },
        { nshort: "M",     nlong: "maj",     names: ["Major triad", "V"],                       pitches: [ 0, 4, 7] },
        { nshort: "+",     nlong: "aug",     names: ["Augmented", "Augmented triad", "Augmented dominant chord"], 
                                                                                                pitches: [ 0, 4, 8 ] },
        { nshort: "sus4",  nlong: "sus4",    names: ["Sus4"],                                   pitches: [ 0, 5, 7 ] },


        // sixth

        { nshort: "m6",    nlong: "min6",    names: ["Minor sixth", "Minor major sixth"],       pitches: [ 0, 3, 7, 9 ] },
        { nshort: "6",     nlong: "maj6",    names: ["Major sixth", "Add6", "M6"],              pitches: [ 0, 4, 7, 9 ] },
        // Dominant chords with 6th are called 13th chords
        { nshort: "Ger+6", nlong: "geraug6", names: ["German augmented sixth"],                 pitches: [ 0, 4, 7, 10 ] },
        { nshort: "Fr+6",  nlong: "fraug6",  names: ["French augmented sixth"],                 pitches: [ 0, 4, 6, 10 ] },
        { nshort: "It+6",  nlong: "itaug6",  names: ["Italian augmented sixth"],                pitches: [ 0, 4, 10 ] },
        { nshort: "N6",    nlong: "neap6",   names: ["Neapolitan sixth"],                       pitches: [ 1, 5, 8 ] },

        // seventh

        { nshort: "o7",    nlong: "dim7",    names: ["Diminished seventh"],                     pitches: [ 0, 3, 6, 9 ] },
        { nshort: "m7-5",  nlong: "min7flat5",names: ["Half-diminished seventh"],                pitches: [ 0, 3, 6, 10 ] },
        { nshort: "oM7",   nlong: "dimmaj7", names: ["Diminished major seventh"],               pitches: [ 0, 3, 6, 11 ] },
        { nshort: "m7",    nlong: "min7",    names: ["Minor seventh"],                          pitches: [ 0, 3, 7, 10 ] },
        { nshort: "mM7",   nlong: "minmaj7", names: ["Minor major seventh"],                    pitches: [ 0, 3, 7, 11 ] },
        { nshort: "7-5",   nlong: "7flat5",  names: ["Dominant seventh flat five"],             pitches: [ 0, 4, 6, 10 ] },
        { nshort: "7",     nlong: "7",       names: ["Dominant seventh", "Harmonic seventh"],   pitches: [ 0, 4, 7, 10 ] },
        { nshort: "",      nlong: "",        names: ["Dominant seventh raised ninth vs dominant seventh split third"],
                                                                                                pitches: [ 0, 4, 7, 10, 15 ] },
        { nshort: "M7",    nlong: "maj7",    names: ["Major seventh"],                          pitches: [ 0, 4, 7, 11 ] },
        { nshort: "+7",    nlong: "aug7",    names: ["Augmented seventh", "7+5", "7#5"],        pitches: [ 0, 4, 8, 10 ] },
        { nshort: "+M7",   nlong: "augmaj7", names: ["Augmented major seventh"],                pitches: [ 0, 4, 8, 11 ] },

        // addX

        { nshort: "add2",  nlong: "add2",    names: ["add2"],                                   pitches: [ 0, 2, 4, 7 ] },
        { nshort: "add9",  nlong: "add9",    names: ["add9"],                                   pitches: [ 0, 4, 7, 14 ] },

        // ninth

        { nshort: "m6/9"   nlong: "min6add9",names: ["Minor 6-9"],                              pitches: [ 0, 3, 7, 9, 14 ] },
        { nshort: "m9",    nlong: "min9",    names: ["Minor ninth"],                            pitches: [ 0, 3, 7, 10, 14 ] },
        { nshort: "9-5",   nlong: "9flat5",  names: ["Ninth flat five"],                        pitches: [ 0, 4, 6, 10, 14 ] },
        { nshort: "6/9",   nlong: "6add9",   names: ["6add9"],                                  pitches: [ 0, 4, 7, 9, 14 ] },
        { nshort: "7-9",   nlong: "7flat9",  names: ["Dominant minor ninth"],                   pitches: [ 0, 4, 7, 10, 13 ] },
        { nshort: "9",     nlong: "9",       names: ["Dominant ninth"],                         pitches: [ 0, 4, 7, 10, 14 ] },
        { names:  "M9",    nlong: "maj9",    names: ["Major ninth"],                            pitches: [ 0, 4, 7, 11, 14 ] },
        { named:  "M7#11", nlong: "maj7sharp11",names: ["Lydian"],                              pitches: [ 0, 4, 7, 11, 18 ] },
        { nshort: "9+5",   nlong: "9aug5",   names: ["Ninth augmented fifth"],                  pitches: [ 0, 4, 8, 10, 14 ] },
        { nshort: "M7#11", nlong: "maj7sharp11",names: ["Major seventh sharp eleventh", "Lydian augmented"],
                                                                                                pitches: [ 0, 4, 8, 16, 18 ] },
        { nshort: "7susp4",nlong: "",        names: ["Seventh suspension four"],                pitches: [ 0, 5, 7, 10 ] },

        // eleventh

        { nshort: "m11",   nlong: "min11",   names: ["Minor eleventh"],                         pitches: [ 0, 3, 7, 10, 14, 17 ] },
        { nshort: "11",    nlong: "11",      names: ["Dominant eleventh"],i                     pitches: [ 0, 4, 7, 10, 14, 17 ] },
        { nshort: "+11",   nlong: "aug11",   names: ["Augmented eleventh"],                     pitches: [ 0, 4, 7, 10, 14, 18 ] },
        // is there an M11?
        { nshort: "M11",   nlong: "maj11",   names: ["Major eleventh", "Eleventh diatonic"],    pitches: [ 0, 4, 7, 11, 14, 17 ] },

        // thirteenth

        { nshort: "m13",   nlong: "min13",   names: ["Minor thirteenth"],                       pitches: [ 0, 3, 7, 10, 14, 17, 21 ] },
        { nshort: "13",    nlong: "13",      names: ["Dominant thirteenth"],                    pitches: [ 0, 4, 7, 10, 14, 17, 21 ] },
        // is there an augmented thirteenth, +13? (likely 0, 4, 7, 10, 14, 18, 21 ?)
        
        { nshort: "M13",   nlong: "maj13",   names: ["Major Thirteenth"],                       pitches: [ 0, 4, 7, 11, 14, 18, 21 ] },
        { nshort: "13#11", nlong: "13sharp11", names: ["Thirteenth sharp eleven"],              pitches: [ 0, 4, 8, 10, 14, 18, 21 ] }, // are these pitches correct?

/*
        // Alternate names; seem to shadow triads for the most part?

        { nshort: "o", nlong: "dim", names: ["Leading tone and tonic"], pitches: [ 0, 3, 6 ] },
        { names: ["Secondary leading-tone"], pitches: [ 0, 3, 6 ] },
        { names: ["Secondary supertonic"], pitches: [ 0, 3, 7 ] }, // enharmonic with minor triad
        { names: ["Subdominant and tonic"], pitches: [ 0, 4, 7 ] },
        { names: ["Subdominant parallel"], pitches: [ 0, 3, 7 ] },
        { names: ["Tonic parallel"], pitches: [ 0, 3, 7 ] },
        { names: ["Tonic counter parallel"], pitches: [ 0, 3, 7 ] },
        { names: ["Subtonic and tonic"], pitches: [ 0, 4, 7 ] },
        { names: ["Dominant parallel"], pitches: [ 0, 3, 7 ] },
        { names: ["Supertonic and tonic"], pitches: [ 0, 3, 7 ] },
        { names: ["Ii-V-I turnaround"], pitches: [ 0, 4, 7 ] },

        // chords specific to certain pieces or composers; we can probably lose these?

        { names: ["So What"], pitches: [ 0, 5, 10, 15, 19 ] },
        { names: ["Magic"], pitches: [ 0, 1, 5, 6, 10, 12, 15, 17 ] },
        { names: ["Mu"], pitches: [ 0, 2, 4, 7 ] },     // associated with Steely Dan? 
        { nshort: "",      nlong: "",        names: ["Farben chord"], pitches: [ 0, 8, 11, 16, 21 ] },
        { nshort: "elektra", nlong: "elektra", names: ["elektra"], pitches: [ 0, 7, 9, 13, 16 ] },
        { nshort: "dream", nlong: "dream",   names: ["Dream"], pitches: [ 0, 5, 6, 7 ] },
        { names: ["Northern Lights"], pitches: [ 1, 2, 8, 12, 15, 18, 19, 22, 23, 28, 31 ] },
        { names: ["Petrushka Second Tableau"], pitches: [ 0, 1, 4, 6, 7, 10 ] },
        { names: ["Wagner Tristan opening"], pitches: [ 0, 3, 6, 10 ] },
        { names: ["Viennese trichord"], pitches: [[ 0, 1, 6], [0, 6, 7]] },
*/
];

var minor_scale = [0, 2, 3, 5, 7, 8, 10];
var major_scale = [0, 2, 4, 5, 7, 9, 11];

