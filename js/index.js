//
// first attempt at hacking together a music trainer in JavaScript
// (C) Jacques Mattheij 2020; jacques@modularcompany.com
// All rights reserved, 
// 
// This project builds on WebMIDI which is not supported in all browsers
// so for now you'll have to use Chrome (sorry.). Another important 
// component is VexFlow, a musical score rendering library.
//
// The idea here is to first make a web browser based replacement for
// PianoBooster, then to turn it into a full fledged course to learn
// how to play the piano, with automatic lesson plans and the ability
// to upload your own MIDI files of pieces that you want to learn how
// to play. I want it to be web based so progress and analysis can be
// done server side and data can be persisted without the user having
// to worry about that, another important reason is to be as much 
// multi platform as possible.
//
// Note that I absolutely loathe javascript, it is a terrible language
// but in the interest of portability and transparency as well as to 
// reduce the number of dependencies in building this I will use straight
// js for the whole project. This also opens up the project to far
// more people as potential contributors.
//
// One reason in particular that I seriously dislike javascript is
// because computer programming would be hard enough if it weren't
// for asynchronous code to our lives even harder, and javascript
// is a never ending hodgepodge of callbacks, promises and other
// nonsense. It allows for some interesting effects at the expense
// of *very easily* losing track of what is going on with your code
// which even for experienced programmers (40 years and counting)
// can be a source of distraction and frustration. I've written
// an operating system and some pretty complex interrupt driven
// device drivers, so it's not as if real time or async stuff 
// scares me. Even so, javascript can turn a simple piece of software
// into something insanely hard to debug. Even mundane operations
// such as persisting a record into a database are done asynchronously
// which really does not help if you are trying to get some work done.
// It's Callback Hell.
//
// This is not the most elegant code when it comes to decoupling 
// presentation and logic, one reason is that there are a lot of
// time constraints, another that I don't see many reasons why the
// presentation layer would ever change, it either works or it 
// doesn't and so far it seems it will work. 
//
// For now I've abandoned the idea of importing music XML, instead 
// we will import MIDI directly from binary files
//
// musicxml may be overkill, possibly much better to read the midifile directly?
//
// That way existing midi files can be used as input to the trainer.
//
// Another idea is to use flashcards and spaced repetition to train
// sightreading skills
//
// Finally, an ear training module could play notes and chords
// which the user would then have to duplicate. 
//
// Including jquery for one call is also overkill, probably should 
// get rid of that and code up a small replacement function to erase
// the score tree in the SVG box
//
// One tricky bit in music notation is that due to 'sharps' and 'flats'
// one pitch value (say 61) can be more than one note in notation, for
// instance a C# or a Db 
//
// About those ticks...
// 
// 'tick' has to be the most overloaded term in this project. Midi files
// use 'ticks' to indicate beats of a clock that are on a much finer
// grained time scale than a note or a bar. A typical quarter note in
// 'midi time' can be anywhere from 10 to 480 ticks from what I've seen
// in practice. Depending on the tempo, so this is actually a variable
// amount of time.
//
// Another way in which the word tick is used in this program is in the
// context of VexFlow, the music score rendering library. VexFlow calls
// everything that is a part of a stave a 'tickable', which means that
// some measure of time is passing while you're looking at it. This is
// opposed to decoration such as clefs, bar lines and so on. But rests 
// and anything related to notes and the notes themselves are tickables.
//
// Fortunately - so far - there is no link with insects or this program
// would be really impossible to understand. But give it some time ;)

console.log("version 0.10");

if (window.location.hostname == 'www.pianojacq.com') {
        // redirect to naked domain
        window.location.replace("https://pianojacq.com");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Lovefield based persistence layer:
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// A Warning on using this for those who are used to other databases.
// There is a bunch of caching between your function calls to lovefield
// and the storage engine, on top of that not all functions that you
// have executed see their effects reflected across all of the underlying
// layers. This can have some very surprising - and bad - effect, such
// as inserting a record, querying that same table and not finding your
// record there. Refreshing the page will invariably turn it up so it
// definitely does get saved. There are a number of workarounds in the
// code to deal with those effects, the regular method of adding
// promises to asynchronous updates are not going to solve this problem.

// Deletion has similar issues, if you delete something there is no guarantee
// that it won't turn up again on a subsequent query. 

// See also:

// https://github.com/google/lovefield/blob/master/docs/spec/05_transaction.md#52-implicit-transaction
// https://stackoverflow.com/questions/15492314/why-is-indexeddb-not-showing-new-entries-in-chrome
// https://stackoverflow.com/questions/15151242/running-code-only-after-an-object-is-updated-in-indexeddb-particularly-in-chrom

// Of course it makes perfect sense to make your database 
// ascynchronous. /not.

// set up the database schema and the database connection

// update this version number whenever you add a table

const dbversion = 10;

var db_connected = false;

// Because it's impossible to query whether or not a database
// exists before opening it we are stuck with the old name,
// even for new installations. 

var schemaBuilder = lf.schema.create('practicekeyboard', dbversion);

// table definitions

// The configuration table holds all of the config
// data that is not specific to a particular piece. 
// it allows for recovery from a page reload to get
// you back to the same midi device and practice mode.
// The filename is used to reload the file you were
// practing with before the refresh or reload of 
// the page. It would probably be better to store 
// the 'hand' information with the scores but that
// makes it harder to do things like playing through
// a number of songs with the right hand in a row. 

schemaBuilder.createTable('Config').
    addColumn('id', lf.Type.INTEGER).
    addColumn('midiin', lf.Type.INTEGER).       // the index of the midi device used for sound input
    addColumn('midiout', lf.Type.INTEGER).      // the index of the midi device used for sound generation
    addColumn('mode', lf.Type.INTEGER).         // the mode (listening, following)
    addColumn('hand', lf.Type.INTEGER).         // the hand that is active
    addColumn('filename', lf.Type.STRING).      // the name of the file we are currently practicing
    addColumn('midilow', lf.Type.INTEGER).      // the lowest value the keyboard can generate
    addColumn('midihigh', lf.Type.INTEGER).     // the highest midi value the keyboard can generate
    addColumn('diff', lf.Type.INTEGER).         // the difficulty factor, 1 = just single notes, two is two notes at the same time, three is triads, four = everything
    addPrimaryKey(['id']);

// The scores table holds all of the per-score information,
// including the score itself, the statistics and the position of the range
// slider when you were practicing this piece last. 
// The stats are a big chunk of json on a per-tick basis,
// an array of 10 slots for each tick. This drives the 
// graphical stats for the score. 

schemaBuilder.createTable('Scores').
    addColumn('filename', lf.Type.STRING).
    addColumn('midi', lf.Type.STRING).
    addColumn('stats', lf.Type.STRING).
    addColumn('start', lf.Type.INTEGER).
    addColumn('last', lf.Type.INTEGER).
    addPrimaryKey(['filename']);

// The practices table holds an entry for each practice
// session that was performed from beginning to end. Interrupted
// or modified practices are not recorded because that would make
// comparing them across practices impossible. Each practice set
// is unique for a given section and hand of a particular midi file.

// This allows for a quick comparison with previous practices
// like the current one, and will show whether you are improving or not.

schemaBuilder.createTable('Practices').
    addColumn('id', lf.Type.INTEGER).
    addColumn('when', lf.Type.DATE_TIME).
    addColumn('filename', lf.Type.STRING).
    addColumn('starttick', lf.Type.INTEGER).
    addColumn('hand', lf.Type.INTEGER).
    addColumn('endtick', lf.Type.INTEGER).
    addColumn('errors', lf.Type.INTEGER).
    addColumn('good', lf.Type.INTEGER).
    addColumn('late', lf.Type.INTEGER).
    addColumn('took', lf.Type.INTEGER).
    addColumn('speed', lf.Type.INTEGER).
    addPrimaryKey(['id'], true).
    addIndex('idxpractice', ['filename', 'starttick', 'endtick', 'hand'], false, lf.Order.DESC);

function objlist(n,o)

{
        var r = [];

        for (var i=0;i<n;i++) {
                r.push(o);
        }

        return r;
}

var blank_per_tick_stat = objlist(stat_history, { errors: 0, delay: 0, good: 0});

// statistics per chord / note

var database;

schemaBuilder.connect().then(function(db) {
        database = db;

        // console.log(database);

        db_now_connected();
});

// configuration table

function config_persist(mode, hand, diff, midiin, midiout, filename, midi_low, midi_high)

{
        var config = database.getSchema().table('Config');

        var row = config.createRow({
                'id': 0,
                'midiin': midiin,
                'midiout': midiout,
                'mode': mode,
                'hand': hand,
                'diff': diff,
                'filename': filename,
                'midilow': midi_low,
                'midihigh': midi_high
        });

        database.insertOrReplace().into(config).values([row]).exec();
}

function config_retrieve()

{
        var config = database.getSchema().table('Config');

        // there really should only be one config record

        database.select().from(config).where().exec().then(function(results) {
                results.forEach(function(row) { config_load(row) });
        });
}

function score_persist(filename, score_midi, stats, start, last)

{
        if (score_midi == null) {
                return;
        }

        // console.log("persisting: ", filename);

        var json_score = JSON.stringify(score_midi);
        var json_stats = JSON.stringify(stats);

        var scores = database.getSchema().table('Scores');

        var row = scores.createRow({
                'filename': filename,
                'midi': json_score,
                'stats': json_stats,
                'start': start,
                'last': last
        });

        // console.log("scores: ", scores, " row " , row);

        database.insertOrReplace().into(scores).values([row]).exec();

        // console.log("persisted");
}

function score_update_midi(filename, score_midi)

{
        if (score_midi == null) {
                return;
        }

        var json_score = JSON.stringify(score_midi);

        var scores = database.getSchema().table('Scores');

        console.log(score_midi);

        database.update(scores).set(scores.midi, json_score).where(scores.filename.eq(filename)).exec().then(function() {
                // in case the file is the same file that we already have in memory
                // and nothing else changes make sure we will do a redraw otherwise
                // it will be quite confusing because the update will not show on
                // the screen until the next page refresh.

                redraw_force = true;

                // reload the score from the db to synchronize what the user sees with that is in the db

                score_retrieve(filename);
      });
}

// @AF this would have been the preferred spot for that delete

function score_delete(filename)

{
        console.log("deleting ", filename);
}

function score_retrieve(filename)

{
        var scores = database.getSchema().table('Scores');

        database.select().from(scores).where(scores.filename.eq(filename)).exec().then(function(results) {
                results.forEach(function(row) { 
                        score_load(row.filename, JSON.parse(row.midi), JSON.parse(row.stats), row.start, row.last);
                });
        });
}       

function pieces_retrieve()

{       
        var scores = database.getSchema().table('Scores');
        
        database.select().from(scores).where().exec().then(function(results) {
                pieces = [];

                results.forEach(function(row) {
                        pieces.push(row.filename);
                });

                populate_file_select();
        });
}

// statslog holds all the statslog records for the current piece from the current starttick to the current endtick

var practice_log = [];

function practice_log_retrieve(filename, starttick, endtick, practice_hand)

{
        console.log("practice_log retrieve: ", filename, starttick, endtick, practice_hand);

        // console.log("retrieving practices");

        if (!db_connected) {
                return;
        }

        var practices = database.getSchema().table('Practices');

        database.select().from(practices).where(lf.op.and.apply(null, [practices.filename.eq(filename), practices.starttick.eq(starttick), practices.hand.eq(practice_hand)])).orderBy(practices.when, lf.Order.DESC).exec().then(function(results) {
                // console.log(results);

                // we zero the list here in case we get called twice in one 
                // tick, that way one of the two will end up visible instead of
                // a double helping.

                practice_log = [];

                results.forEach(function(row) {
                        // console.log("row: ", row);

                        practice_log.push(row);
                });
                show_practice_log();
        });
} 

function format_date(d)

{
        var year = d.getYear() + 1900;

        var month = d.getMonth()+1;

        var day = d.getDate();

        // Hours part from the timestamp
        var hours = d.getHours();
        // Minutes part from the timestamp
        var minutes = "0" + d.getMinutes();
        // Seconds part from the timestamp
        var seconds = "0" + d.getSeconds();

        // Will display time in 10:30:23 format
        var formatted_time = year + "/" + month + "/" + day + " " + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

        return formatted_time;
}

// turn a numerical version of practice_hand into a textual one for display

function hand_desc(v)

{
        return ['left', 'right', 'both'][v-1];
}

// this function should move out of the persistence layer; it is a display function.
// show the practice log entries for similar sessions

function show_practice_log()

{
        // console.log(practice_log.map(logentry => 'file: ' + logentry.filename + '<br/>').join(''));

        $("#practicelog").html(
                practice_log.map(l => 
                        format_date(l.when) + ' ' + l.filename + "[" + l.starttick + "," + l.endtick + "," + hand_desc(l.hand) + "] (" + l.good + " out of " + (l.good + l.errors) + ") (" + l.errors + " wrong) (" + l.late + " late) (early ???) (jitter ???? ms/note) (took " + l.took + ") delay ??? tempo " + l.speed + " %<br/>").join('')
       );
}

var practice_persisted = false;

function practice_persist(filename, when, starttick, endtick, errors, good, late, took, speed, hand)

{
        // check to see if the whole practice was completed
        // if it wasn't (skipped or repeated bits) then we 
        // do not log a practice session
        
        if ((endtick-starttick)+1 != (good+errors)) {
                console.log("not a complete practice");
                // Disabled this for now to ensure practices are saved
                // TODO

                // return;
        }

        // don't ever log the same practice session twice.

        if (practice_persisted) {
                console.log("already persisted this practice!");
                return;
        }

        practice_persisted = true;

        console.log("persisting practice");

        // add the entry to the db.

        var practices = database.getSchema().table('Practices');

        var row = practices.createRow({
                'id': 0,
                'when': when,
                'filename': filename,
                'starttick': starttick,
                'endtick': endtick,
                'hand': hand,
                'errors': errors,
                'good': good,
                'late': late,
                'took': took,
                'speed': speed
        });

        console.log("row: ", row);

        database.insertOrReplace().into(practices).values([row]).exec();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// The autopilot, this guides the user through practicing a new piece until they have mastered it.
// Ideally this would be completely stateless and re-starting it from any point in a practice
// would have the exact same effect.

var bar_scores = [];
var bar_average = 0;
var worst_bar   = 0;

function per_tick_stats_bar_score(score, tickindices, per_tick_stats)

{
	var n = 0;
	var good = 0;
	var errors  = 0;
	var delay = 0;
        var have_notes = false;

	for (var i=0;i<tickindices.length;i++) {
		if (score.n_notes_for_hand_at_tickindex(config_hand, tickindices[i]) > 0) {
                        have_notes = true;

			// console.log("tickindex: ", tickindices[i], "is active");
                        // console.log("pts: ", per_tick_stats);

                        // we can't calculate the stats if there is no data

                        if (typeof(per_tick_stats[tickindices[i]]) == "undefined") {
                                continue;
                        }

                        // we have some data, tally the various counts 

			for (var j=0;j<5 && j<per_tick_stats[tickindices[i]].length;j++) {
				stats = per_tick_stats[tickindices[i]][j];

				// console.log(stats);

                                // we don't actually use the 'good' count for anything.

                                var f = 5-j;

				if (stats.good) {
					good += f;
                                        n += f;
				}

				if (stats.delay) {
					delay += f;
                                        n += f;
				}

				if (stats.errors) {
					errors += f;
                                        n += f;
				}
			}
		}
	}

        // avoid dividing by zero

	if (n == 0) {
                if (have_notes) {
                        // we have notes but none got played so this is a very bad bar
		        return 0.0;
                }
                else {
                        // we don't have notes so this bar is always perfect
                        return 1.0;
                }
	}

        // return a value between 0 and 1 to indicate how good this
        // bar was

        // ideally, not having studied a bar and playing it would give you a low 20% 
        // or so score which would then go up bit by bit as you approach perfection
        // say we have a bar of 10 ticks with 5 data points for each tick. That gives
        // 50 chances for a good note, an error or a delayed note.

        // if all of them are bad then that's 0 out of 50 or 0% score
        // if 25 are good, 10 are wrong and 15 are late that should be
        // somewhere between 50 and 100%

        console.log("   good ", good, "delay ", delay, "n ", n, "score " , (good / n) + (delay / (n * 2)));

        return (good / n) + (delay / (n * 2));

        // older score formulas

        // return 1 - (((errors * .8) + (delay * .2)) / n);

	// return 1 - ((errors+(delay/32)) / n);
}

// compute the score for all bars and order them by increasing
// score

function per_tick_stats_bar_scores(score, per_tick_stats)

{
	var scores = [];

	for (var bar=0;bar<score.bars.length;bar++) {
                console.log("bar: ", bar);
		scores.push({ bar: bar, score: per_tick_stats_bar_score(score, score.bars[bar].tickindices, per_tick_stats) });
	}

	scores = scores.sort((a, b) => a.score - b.score);

	console.log("per bar scores: ", scores);

	return scores;
}

// return the bar number with the worst score of all bars

function per_tick_stats_worst_bar(bar_scores)

{
	if (bar_scores.length == 0) {
		return 0;
	}

	return bar_scores[0].bar;
}

// return average score across all bars

function per_tick_stats_average_bar_score(bar_scores)

{
        var sum = 0;

        console.log(bar_scores);
        
        if (bar_scores.length == 0) {
                return 1.0;
        }

        for (var i=0;i<bar_scores.length;i++) {
                sum += bar_scores[i].score;
        }

        console.log("sum", sum);

        return sum / bar_scores.length;
}

function per_tick_stats_compute_scores(score, per_tick_stats)

{
        bar_scores = per_tick_stats_bar_scores(score, per_tick_stats);

        // figure out which bar is the worst

        worst_bar = per_tick_stats_worst_bar(bar_scores);

        bar_average = per_tick_stats_average_bar_score(bar_scores);

        // show the user how far they are with this piece

        var pscore = (Math.floor(bar_average*10000) / 100);
        $("#completion").text(pscore + "%");

        // draw a chart to make it more visual
        // TODO get rid of elseif mess
        let cirlce = document.getElementById("circle");
        cirlce.setAttribute("stroke-dasharray", pscore+", 100");
        if (pscore > 90) { 
                cirlce.style.stroke = '#69B34C';
        } else if (pscore > 75) {                
                cirlce.style.stroke = '#ACB334';        
        } else if (pscore > 50 ) {
                cirlce.style.stroke = '#FAB733';
        } else if (pscore > 25 ) {
                cirlce.style.stroke = '#FF8E15';
        } else if (pscore > 15 ) {
                cirlce.style.stroke = '#FF4E11';                
        } else {
                cirlce.style.stroke = '#FF0D0D';
        }

}

// check if we have enough data to do an autopilot run, occassionally
// will return true randomly to force the player to go through the 
// whole piece.

var n_playthroughs = 0;

function null_stats(stats)

{
	if (typeof(stats) == "undefined") {
		return true;
	}

	if ((stats.delay + stats.good + stats.errors) == 0) {
		return true;
	}

	return false;
}

function autopilot_should_do_playthrough(score, per_tick_stats)

{
	// ensure coverage

	for (var i=0;i<score.last_tickindex;i++) {
		if (score.n_notes_for_hand_at_tickindex(config_hand, i) > 0) {
			if (null_stats(per_tick_stats[i])) {
				return i;
			}
	 	}
	}

	// but for now we always return false

	return -1;
}

// the autopilot_practice_range function is called once per 
// practice run (usually at the end, but on a mode switch
// once at the beginning). 

function autopilot_practice_range(score, per_tick_stats)

{
	console.log("autopilot_practice_range!");

	if (score == null) {
		console.log("no score!");
		console.log(score);
		return;
	}

	console.log("score.bars.length", score.bars.length);

	// if there are no bars then there is no work.

	if (score.bars.length == 0) {
		console.log("no bars");

		return;
	}

        per_tick_stats_compute_scores(score, per_tick_stats);

	// figure out if we have enough data to work with, if not we will
	// command a playthrough

	var start = 0;

	if ((start = autopilot_should_do_playthrough(score, per_tick_stats)) >= 0) {
		console.log("not enough data yet, starting playthrough at ", start);
		start_tickindex = start;
		last_tickindex = score.last_tickindex;
	}
	else {
		// we have enough data to determine where the student is still weak

		var start_bar = worst_bar - 1;
		var end_bar = worst_bar + 1;

                // depending on what the worst bar scores like we will now
                // do 3, 5, 7 or 9 bars of the piece (the closer we get to
                // perfection the longer the segments you get to practice)

                if (bar_scores[worst_bar].score > 0.7) {
                        start_bar--;
                        end_bar++;
                }

                if (bar_scores[worst_bar].score > 0.8) {
                        start_bar--;
                        end_bar++;
                }

                if (bar_scores[worst_bar].score > 0.9) {
                        start_bar--;
                        end_bar++;
                }

                if (bar_scores[worst_bar].score > 0.95) {
                        start_bar--;
                        end_bar++;
                }

		if (start_bar < 0) {
			start_bar = 0;
		}

		if (end_bar >= score.bars.length) {
			end_bar = score.bars.length-1;
		}

                console.log("start_bar: ", start_bar, "end_bar: ", end_bar);

                if (start_bar > end_bar) {
                        debugger;
                }

		start_tickindex = score.bars[start_bar].tickindices[0];

                console.log(score.bars[start_bar].tickindices);

		last_tickindex  = score.bars[end_bar].tickindices[
				  score.bars[end_bar].tickindices.length-1
			];

                // protect against start bars without content

                if (typeof(start_tickindex) == 'undefined') {
                        start_tickindex = 0;
                }

                if (start_tickindex >= last_tickindex) {
                        debugger;
                }

                if (typeof(last_tickindex) == 'undefined') {
                        debugger;
                }

                console.log("the worst bar is ", worst_bar, "at ", bar_scores[worst_bar].score , " average is ", bar_average * 100, "%");
	}

	rewind();

	console.log("autopilot done");
}

// The init function is called once upon a mode change
// it can be used to re-initialize the autopilot to a 
// known 'safe' state upon re-engagement.

function autopilot_init(score, per_tick_stats)

{
        per_tick_stats_compute_scores(score, per_tick_stats);

        if (config_mode == automatic) {
	        autopilot_practice_range(score, per_tick_stats);
        }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

// user facing messages box

function message_user(m) 

{  
        SimpleToast.info(m);
}

// Simple Toast class that allows for warning and info modes to have different classes.
// Usage: SimpleToast.warn("Message") or SimpleToast.info("Message")
let SimpleToast = (function() {

        info = function(m) {
                showToast(m, "toastinfo");
        }

        warn = function(m) {
                showToast(m, "toastwarn");

        }

        let showToast = function(m, css) {

                $("#info").text(m);
                $("#info").attr('class', css);
                $("#info").fadeIn('fast');
        
                // remove the info box again after the user has had a chance to read it
        
                window.setTimeout(function () {
                        $("#info").fadeOut('fast');
                }, 5000);
        };
        return self;

})();


// get rid of horizontal scrollbar for the whole document

document.documentElement.style.overflowX = 'hidden';

// the list of pieces that can be chosen for practice
// this is used to populate the drop down menu in the ui

var pieces = [];

// the current score; this is the contents of the midi file

var score_midi = null;
var score_filename = null;

// the instance variable for the current score as displayed on the canvas

var score = null;

// the keyboard range as specified by the user
// eventually this should be stored serverside or recovered when the user logs in

// alternatively; we could have shortcuts for 25, 32, 49, 61, 73, 76, 85 and 88 key keyboards

var kbd_midi_low  = 21;
var kbd_midi_high = 108;

// The 'mode' variable describes the overall mode of operation of
// the program, it makes the highest level changes to the interaction
// between the user and the software. 

var config_mode = following;   // this should mirror the default in index.html

function mode_change()

{
        config_mode = $("#mode").val();

        config_save();

        state = paused;

	autopilot_init(score, per_tick_stats);

        play_button();
}

function diff_change()

{
        config_diff = $("#diff").val();

        config_save();

        redraw();
}

// state indicates the state of the scroller and if activated the player
// and keeps the music and the score synchronized and animated. When 'paused'
// we are typically waiting for the user to press 'play' or to play notes. 

var state = paused;

var scrolling = 0;  // positive when the scrollbar was just active, used to suppress midi output

var config_hand = bothhands;

var config_diff = 4;    // by default show and expect all notes

var config_midiin = 1;
var config_midiout = 1;

// var config_paper = "#e0d3af";// old manuscripts
var config_paper = "white";     // boring white
//var config_paper = "beige";     // a bit less boring than white

// The width of the canvases is chosen in such a way that a canvas can hold *at least*
// one complete bar. If more can be squeezed in we do so. 

const canvas_width = 4000;      // width of one canvas section of the score

// each canvas that we are displaying has a position and a width

var score_canvases = [
        { pos: 0, width: 0, canvas: "vexsvg_00" },
        { pos: 0, width: 0, canvas: "vexsvg_01" },
        { pos: 0, width: 0, canvas: "vexsvg_02" },
        { pos: 0, width: 0, canvas: "vexsvg_03" },
        { pos: 0, width: 0, canvas: "vexsvg_04" },
        { pos: 0, width: 0, canvas: "vexsvg_05" },
        { pos: 0, width: 0, canvas: "vexsvg_06" },
        { pos: 0, width: 0, canvas: "vexsvg_07" },
        { pos: 0, width: 0, canvas: "vexsvg_08" },
        { pos: 0, width: 0, canvas: "vexsvg_09" },
        { pos: 0, width: 0, canvas: "vexsvg_10" },
        { pos: 0, width: 0, canvas: "vexsvg_11" },
        { pos: 0, width: 0, canvas: "vexsvg_12" },
        { pos: 0, width: 0, canvas: "vexsvg_13" },
        { pos: 0, width: 0, canvas: "vexsvg_14" },
        { pos: 0, width: 0, canvas: "vexsvg_15" },
        { pos: 0, width: 0, canvas: "vexsvg_16" },
        { pos: 0, width: 0, canvas: "vexsvg_17" },
        { pos: 0, width: 0, canvas: "vexsvg_18" },
        { pos: 0, width: 0, canvas: "vexsvg_19" },
        { pos: 0, width: 0, canvas: "vexsvg_20" },
];

var score_xpos = 0;             // the position of the left most canvas (even if we are not actually moving it we still keep it relative to that)
var current_canvas = 0;         // the first canvas we are displaying

var synth = null;

function config_save()

{
        config_persist(config_mode, config_hand, config_diff, config_midiin, config_midiout, score_filename, kbd_midi_low, kbd_midi_high);
}

function score_save()

{
        score_persist(score_filename, score_midi, per_tick_stats, start_tickindex, last_tickindex);
}

// config_load is called async, that's why we need to do all of the ui housekeeping here.

function config_load(row)

{
        // console.log("loading config");

        // console.log(row);

        // set all global configuration variables
        // maybe we should move all of them into a global config struct
        // TODO

        config_loaded = true;

        config_midiin = row.midiin;
        config_midiout = row.midiout;
        config_mode = row.mode;
        config_hand = row.hand;
        config_diff = row.diff;

        // restore midi keyboard size and reflect it in the drop down

        kbd_midi_low = row.midilow;
        kbd_midi_high = row.midihigh;

        $("#selectKeys").val((kbd_midi_high-kbd_midi_low)+1); 

        // ensure that the UI state is in sync with the retrieved settings

        console.log("config_hand: ", config_hand);

        $("#hand").val(config_hand);

        $("#mode").val(config_mode);

        $("#diff").val(config_diff);

        set_output(config_midiout);
        set_input(config_midiin);        

        // console.log("done with retrieving config");

        score_retrieve(row.filename);
}

var had_an_error = false;       // indicates that during the playing of these note(s) the player made an error

// keysignature and scale are pulled from the midi header
// and are used to draw the clef and the key signature on 
// the left hand side of the score display

var keysig = "C";
var scale = "major";

// 3/4 time signature means -> 3 quarter notes per bar (or 'measure')
// 6/8 time signature means -> 6 eighth notes per bar
// so (how many notes)/(of what duration) per bar

var timesig = "4/4";
var timesig_num = 4;
var timesig_type = 4;

// deal with MIDI devices, we need at least an input device

let midiIn = [];
let midiOut = [];

if (navigator.requestMIDIAccess) {
        message_user('WebMIDI is supported in this browser.');
        navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
} 
else {
        // redirect here to a page about WebMiDI

        window.location.href = "firefox.html";

        message_user('WebMIDI is not supported in this browser.');
}

function displayDevices() 

{
  selectIn.innerHTML = midiIn.map(device => `<option>${device.name}</option>`).join('');
  selectOut.innerHTML = midiOut.map(device => `<option>${device.name}</option>`).join('');
}

function onMIDISuccess(midiAccess) 

{
        // Reset.
        midiIn = [];

        // MIDI devices that can send you data.

        const inputs = midiAccess.inputs.values();

        for (let input = inputs.next(); input && !input.done; input = inputs.next()) {
                midiIn.push(input.value);
        }

        // MIDI devices that you can send data to.

        // first we add the builtin tone generator

        midiOut = [{ name: 'Builtin Audio' }];

        // then we add all the midi output devices that we can find.

        const outputs = midiAccess.outputs.values();

        for (let output = outputs.next(); output && !output.done; output = outputs.next()) {
                midiOut.push(output.value);
        }

        // put them all in the drop down

        displayDevices();

        set_input(config_midiin);
        set_output(config_midiout);

        midi_notes_off();        
}

function set_input(selected)

{
        var i = 0;
        for (var input of midiIn) {
                if (i == selected) {
                        input.onmidimessage = getMIDIMessage;
                }
                else {
                        input.onmidimessage = null;
                }
                i++;
        }

        // reflect the change in the UI

        document.getElementById("selectIn").selectedIndex = selected;
}

function set_output(selected) 

{
        document.getElementById("selectOut").selectedIndex = selected;
}

// callback called whenever the drop down selected element is modified

function input_change() 

{
        var selected = document.getElementById("selectIn").selectedIndex;

        config_midiin = selected;

        set_input(selected);

        config_save();
}

function output_change()

{
        var selected = document.getElementById("selectOut").selectedIndex;

        config_midiout = selected;

        if (selected == 0) {
                // start up Tone.js
                start_tone_js();
                console.log("tonejs on");
        }
        else {
                // disable Tone.js
                console.log("tonejs off");
                synth = null;
        }

        config_save();
}

function onMIDIFailure() 

{
        document.querySelector('.note-info').textContent = 'Error: Could not access MIDI devices. Connect a device and refresh to try again.';
}

function sendMidiMessage(message, pitch, velocity) 

{
        if (selectOut.selectedIndex > 0) {
                const device = midiOut[selectOut.selectedIndex];

                device.send([message, pitch, velocity]);
        }
}

// determine whether input and output are done on the
// same midi device

function input_and_output_are_equal()

{
        if (midiOut[selectOut.selectedIndex].name == midiIn[selectIn.selectedIndex].name) {
                return true;
        }

        return false;
}

var midi_to_tone

function midi_play_note(pitch, duration, velocity)

{
        if (selectOut.selectedIndex == 0) {
                // device index 0 is the built in tone generator

                now = Tone.now();
                var note = Tone.Frequency(pitch, "midi").toNote()
                // for some reason a negative value here removes some of the latency that
                // Tone.js introduces. That probably isn't the best solution
                // TODO
                synth.triggerAttack(note, now-0.100);
                synth.triggerRelease([note], now + duration / 1000);
        }
        else {
                const device = midiOut[selectOut.selectedIndex];

                device.send( [0x90, pitch, velocity]);

                // console.log("pitch ", pitch, "duration", duration);

                device.send( [0x90, pitch, 0], window.performance.now() + duration );
        }
}

notes_on = [];
notes_on_midi = [];

function midi_notes_off()

{
        for (var i=0;i<128;i++) {
                notes_on[i] = 0;
                notes_on_midi[i] = 0;

                midi_play_note(i, 0, 0);
        }
}

// we should get rid of this table, it can be computed on the fly for each hand individually
// with a simple formula

notes_y = [ 
//      C  C#   D  D#   E   F  F#   G  G#   A  A#   B
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 0...11
        0,  0,  0,  0,  0,  0,  0,  0,  0,536,531,526,  // 12..23
      516,511,506,501,496,486,481,476,471,466,461,456,  // 24..35
      446,441,436,431,426,416,411,406,401,396,391,386,  // 36..47
      376,371,366,361,356,346,341,336,331,326,321,316,  // 48..59
      306,301,296,291,286,276,271,266,261,256,251,246,  // 60..71
      236,231,226,221,216,206,201,196,191,186,181,176,  // 72..83
      166,161,156,151,146,136,131,126,121,116,111,106,  // 84..95
       96, 91, 86, 81, 76, 66, 61, 56, 51, 46, 41, 36,  // 96..107
       26,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  // 108
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
];

function find_hand_split(keys)

{
        var low_right = 128;
        var high_left = 0;

        // if we are in a single handed mode then the split point shifts to the end
        // of the keyboard. E.g. if in right-hand mode, the split point is far left
        // of the board

        if (config_hand == righthand) {
                return high_left
        } else if (config_hand == lefthand) {
                return low_right
        }

        var split = 60;

        for (var i=0;i<keys.length;i++) {

                // on every key that changes the low and high
                // water marks we also adjust the other hand to
                // start a handspan away if that's lower than 
                // what is already there
                //
                // that makes the notes_on display a lot more
                // quiet already but it still isn't perfect, in
                // some circumstances, especially when there is a
                // long chord held in one hand while the other hand
                // makes large jumps the cursors can jump even though
                // the other hand is immobile. This isn't all that easy
                // to solve the way the note-on cursors are dealt with,
                // in a way they should be tied to a specific note and
                // as long as that note was held they should not be
                // changed.
                //
                // But for now it works well enough, need to revisit
                // this at some point to make it perfect.
                //
                // one way to achieve this is to not show more notes_on
                // cursors than there are expected notes? (maybe not,
                // the expected notes cursors are good visual feedback
                // that a key is pressed)
                //
                // TODO

                if (keys[i].voice == 1) {
                        // key is in the left
                        if (keys[i].midi > high_left) {
                                high_left = keys[i].midi;
                                if (keys[i].midi + 13 < low_right) {
                                        low_right = keys[i].midi + 13;
                                }
                        }
                }
                if (keys[i].voice == 0) {
                        // key is in the right
                        if (keys[i].midi < low_right) {
                                low_right = keys[i].midi;
                                if (keys[i].midi - 13 > high_left) {
                                        high_left = keys[i].midi - 13;
                                }
                        }
                }

                split = (low_right + high_left) / 2;
        }

        // cap the split on what we can reasonably display
        // between the two staves, you can go about an 
        // octave up or down from middle C

        if (split < 49) {
                split = 49;
        }

        if (split > 71) {
                split = 71;
        }

        // console.log("high_left = ", high_left, " low_right = ", low_right, " split = ", split);

        return split;
}

// helper function to figure out in which hand to
// show this note. The expected keys array for the
// current tick is a good indication, the midpoint
// between the lowest note for the right and the
// highest note for the left is the cut-off point

function in_bass(i)

{
        // how is this possible?
        // this seems to trigger when practicing the right hand only
        // TODO

        if (typeof(score.notes_at_tickindex(tickindex)) == "undefined") {
                console.log("should not happen 983");

                return false;
        }

        var split = find_hand_split(score.notes_at_tickindex(tickindex));

        if (i < split) {
                return true;
        }

        return false;
}

// show the keys the user is pressing.

// In pianobooster the noteon bar is dashed, when on a flat or sharp,
// that is a good hint because otherwise it is visually hard to see
// TODO

// yet another problem is that some notes can be present in more than
// one location; for instance Central C can be shown on the treble
// and on the bass clef, which one to show depends on which hand the
// user uses to strike the key, which we can not know for sure!

// if a note is present in the expected set for a particular hand
// then we should definitely show it at that position.
// TODO

function show_notes_on()

{
        var n = 0;
        var o = 0;

        for (var i=0;i<128;i++) {
                if (notes_on_midi[i]) {

                        // depending on whether we show the cursor in the
                        // treble or bass part we will offset the position

                        // the cut-off is the halfway mark between the
                        // highest note in the left hand and the lowest note
                        // in the right hand 
                        //
                        // this is still not perfect. For instance, after
                        // playing a single note in the left hand, a chord
                        // in the right hand and then the *next* tick
                        // will have only a single note in the left hand
                        // then all notes will jump because they will be
                        // shown as active in the left hand whereas they
                        // are simply still held in the right.

                        if (in_bass(i)) { 
                                o = 72; // bass staff offset - -8 adjusted for original body margin now 0
                        }
                        else {
                                o = -8;  // treble staff offset- -8 adjusted for original body margin now 0
                        }

                        var color = "green";

                        if (!expected(i, score.notes_at_tickindex(tickindex))) {
                                color = "red";
                        }

                        $("#noteon_" + n).css({ top: (o+notes_y[i]) + 'px',
                                                background: color
                        });
                        $("#noteon_" + n).show();

                        n++;
                }
        }

        // hide all noteon_X cursors that we are not currently using

        for (;n<10;n++) {
                $("#noteon_" + n).hide();
        }
}

// Receive a MIDI messages from the attached MIDI input device, 
// this keeps track of which keys are down right now on the keyboard
// and will call the matcher to see if those keys match the expected
// keys in 'following' mode. 

function getMIDIMessage(message) {
        var command = message.data[0];
        var note = message.data[1];
        var velocity = message.data[2];

        switch (command) {
                case 144: // noteOn
                        if (velocity > 0) {
                                notes_on[note] = 1;
                                notes_on_midi[note] = 1;

                                if (synth != null) {
                                        // this will need some more subtlety, for instance
                                        // right now you can't hold a key down, release it
                                        // and have the expected result
                                        // TODO

                                        midi_play_note(note, 0.5, velocity);
                                }
                        }
                        else {
                                notes_on[note] = 0;
                                notes_on_midi[note] = 0;
                        }

                        break;
                case 128:       // note Off
                        notes_on[note] = 0;
                        notes_on_midi[note] = 0;
                        break;
                case 254:
                case 248:
                        return;
                default:
                        return
                        break;
        }

        // the notes_on state has likely changed, so redraw the notes on cursors

        show_notes_on();

        // send the audio back out to the output device so you can hear what is being played

        if (synth == null) {
                if (!input_and_output_are_equal()) {
                        // if input and output devices are the same then we don't do this
                        // but when they are different we assume that the user is using
                        // a keyboard as their input device and some other synth (virtual
                        // or real) as the output device. That means that they can not hear
                        // their own playing unless we forward the packets to the output
                        // device. When the devices are the same the likely input device
                        // is a midi piano or a synth/controller combo in which case the
                        // device will already make sound when the user plays.

                        sendMidiMessage(command, note, velocity);
                }
        }

        // after a rewind at the beginning of the piece the user can start
        // practicing by hitting the first note. 

        if ((config_mode == following || config_mode == automatic) && state == paused && tickindex == start_tickindex) {
                play_button();
        }

        // test if the user has completed the expected set of notes

        if (state == running || state == waiting) {
                all_expected_notes_on()
        }
}

function play_active_notes(x, hands)

{
        var notes = score.notes_at_x(x);

        for (var i=0;i<notes.length;i++) {
                // TODO hardcoded volume here
                if (!notes[i].grayed) {
                        midi_play_note(notes[i].midi, notes[i].duration, 80);
                }
        }
}

// this will play all the notes the user can not or has 
// chosen not to play on account of what they are practicing
// (left, right) or because the limitations of their keyboard

// this is used in following and automatic mode

function play_grayed_notes(x)

{
        if (state == waiting) {
                return;
        }

        var notes = score.notes_at_x(x);

        if (typeof(notes) == "undefined") {
                return;
        }

        for (var i=0;i<notes.length;i++) {
                // TODO hardcoded volume here
                if (notes[i].grayed) {
                        // we play these grayed notes at reduced volume so the player can hear themselves well
                        midi_play_note(notes[i].midi, notes[i].duration, 50);
                }
        }
}

// a single tick of the clock
// what happens depends on 'mode' and 'status'

var start_tickindex = 0;                // we rewind to this tickindex
var last_tickindex = 1000000;           // the last tick we play

var tickindex = -1;                     // this is where we are in the score 

var per_tick_stats = [];                // player statistics on a per_tick basis for the current piece

var tempo_multiplier = 1.0;

// a note is expected, whether it is grayed out or not does not matter
// used to determine whether we should classify a note as an error

function expected(j, a)

{
        if (typeof(a) == "undefined") {
                return true;
        }

        for (var i=0;i<a.length;i++) {
                // matching note
                if (a[i].midi == j) {
                        return true;
                }
        }

        // no matching note; or not grayed out.

        return false;
}

// a note is expected *and* it is not grayed out
// used to determine whether the expected notes are all played

function expected_active(j, a)

{
        for (var i=0;i<a.length;i++) {
                // matching note
                if (a[i].midi == j && a[i].grayed == false) {
                        return true;
                }
        }

        // no matching note; or not grayed out.

        return false;
}


// check how many notes of this particular spot are not grayed out

function count_active_notes(a)

{
        var n = 0;

        if (typeof(a) == "undefined") {
                return 0;
        }

        for (var i=0;i<a.length;i++) {
                if (!a[i].grayed) {
                        n++;
                }
        }

        return n;
}

// a bad note is one that isn't in the expected array at all

function count_bad_notes(sounding, expected_notes)

{
       for (var i=0;i<sounding.length;i++) {
                if (!expected(sounding[i], expected_notes)) {

                        if (!score.allowed_to_sound(tickindex, sounding[i])) {
                                return true;
                        }
                        else {
                                // this may lead to double strikes being ignored entirely
                                // if true we may need to differentiate between paused
                                // mode and 'running' mode where we only excuse the
                                // note if we are paused to ensure that some kind of 
                                // error is recorded
                                // maybe a TODO ?

                                // for now log this kind of mistake on the console
                                // so we can see how often this happens

                                console.log("pardoning a note that is still sounding");
                        }
                }
        }

        return false;
}

// count the number of notes that are expected; not grayed 

function count_good_notes(sounding, expected_notes)

{
       var n = 0;

       for (var i=0;i<sounding.length;i++) {
                if (expected_active(sounding[i], expected_notes)) {
                        n++;
                }
        }

        return n;
}

var tickstatsrect = document.getElementById("tickstats").getBoundingClientRect();

$("#tickstats").attr('width', tickstatsrect.width);
$("#tickstats").attr('height', tickstatsrect.height);

var tickstatscanvas = document.getElementById("tickstats").getContext("2d");

function gp_fill_rect(x, y , w, h , color)

{
        tickstatscanvas.fillStyle = color;
        tickstatscanvas.fillRect(x,y,w,h);
}

// show some pretty test squares

gp_fill_rect(0,0,10,10, "green");
gp_fill_rect(10,10,10,10, "red");
gp_fill_rect(20,20,10,10, "blue");
gp_fill_rect(30,30,10,10, "pink");

function show_per_tick_stats(tick)

{
        // check if we are asking to draw data that we don't have
        
        if (tick >= per_tick_stats.length) {
                return;
        }

        var rect = document.getElementById("tickstats").getBoundingClientRect();

        var w = rect.width / ((last_tickindex - start_tickindex) + 1);
        var h = rect.height / 10;

        var x = w * (tick - start_tickindex);

        for (var i=0;i<10;i++) {

                var y = i * h;

                var color = "white";

                if (per_tick_stats[tick][i].errors) {
                        color = "red";
                }
                if (per_tick_stats[tick][i].good) {
                        color = "green";
                }
                if (per_tick_stats[tick][i].delay) {
                        color = "lightgreen";
                }

                gp_fill_rect(x, y, w, h, color);        
        }
}

function draw_all_tick_stats()

{
        for (var i=0;i<per_tick_stats.length;i++) {
                show_per_tick_stats(i);
        }
}

var total_errors = 0;
var total_good = 0;
var total_late = 0;
var start_time = 0;
var end_time = 0;

// here we keep track of how long we were waiting after the
// notes we expected turned lightgreen. This can then be
// used to determine whether we will make them lightgreen
// (late) or regular green (on time). Technically any delay
// is too much but the display moves quite fast and it is 
// too hard to get the notes to hit properly every time.

var ticks_waiting = 0;

// total_waiting tracks the total time in the waiting state
// in milliseconds.
// this is used at the end of a practice run to determine
// the speed of the player.

var total_waiting = 0;

// cumulative time practiced

var total_practiced = 0;

var ms_per_tick = 0;    // this is used to determine how long we are in the 'waiting' state during a practice run

function show_stats_counters()

{
        var total_took = Math.floor(total_practiced/100) / 10;

        // $('#statscounters').html(score_filename + " (" + total_good + " out of " + (total_good + total_errors) + ") (" + total_errors + " wrong) (" + total_late + " late) (early ???) (jitter ???? ms/note) (took " + total_took + ") delay ??? tempo ?? %;");
        $('#scorewrong').html(total_errors + "/" + (total_good + total_errors));
        $('#scorelate').html(total_late);
}

// log statistics for a single tick

function per_tick_stats_log(i, e)

{
        // only log stats within the area that we are practicing

        if (i < start_tickindex || i > last_tickindex) {
                return;
        }

        // shift down stats to make room for a new performance

        // we really should shift only the part of the score that we will be practicing!

        // prepend a new blank stats block for this score

        // add a new blank entry at the beginning

        while (per_tick_stats.length <= i) {
                // the midi file was changed or the program was changed, either way, the
                // number of ticks is no longer what it was before, so we extend it.

                per_tick_stats[per_tick_stats.length] = [... blank_per_tick_stat];
        } 

        per_tick_stats[i].unshift(e);

        // drop off the entry at the end

        per_tick_stats[i].pop();

        show_per_tick_stats(i);

        total_errors += e.errors;
        total_good   += e.good + e.delay;
        total_late   += e.delay;

        show_stats_counters();
}

function stats_counters_reset()

{
        start_time = Math.floor(Date.now()/1000);

        total_errors = 0;
        total_good   = 0;
        total_late   = 0;

        total_waiting = 0;

        total_practiced = 0;

        /* show_stats_counters(); */
}

// check to see if the cursor is exactly on a set of notes to be checked
// or played

function on_hotspot(tick_x)

{
        if (tick_x < hairline_x()) {
                return true;
        }

        return false;
}

function all_expected_notes_on()

{
        var n = 0;
        var notes_sounding = [];

        // when playing there are no expected notes

        if (config_mode == listening) {
                return false;
        }

        // when paused we don't do good/bad note detection

        if (state == paused || state == finished) {
                return false;
        }

        if (scrolling) {
                return false;
        }

        for (var i=0;i<128;i++) {
                if (notes_on[i] > 0) {
                        notes_sounding[n++] = i;
                }
        }

        // the condition for a note to be in error is that it isn't to be
        // played at all. Another possible condition is that the note is
        // played; released without completion of a chord. That too should count
        // as an error, but practice should continue uninterrupted. TODO

        var notes = score.notes_at_tickindex(tickindex);

        bad_notes = count_bad_notes(notes_sounding, notes);

        good_notes = count_good_notes(notes_sounding, notes);

        active_notes = count_active_notes(notes);

        if (bad_notes) {
                if (had_an_error == false) {

                        // we only count one error per note or chord

                        score.color_active_noteheads(tickindex, "red");

                        per_tick_stats_log(tickindex, { errors: 1, good: 0, delay: 0});
                }

                had_an_error = true;

                return false;   // have a wrong note
        }

        // The first note played marks the beginning of the practice session so we reset the time
        // this means that if someone waits a few seconds before they begin to play they do not
        // get penalized for that, the first note is always considered to be 'on time' if that
        // is where the practice starts

        if (tickindex == start_tickindex) {
                start_time = Math.floor(Date.now()/1000);
        }

        // the number of notes sounding that are expected is equal to the 
        // number of notes we are expecting in total then we are complete
        // otherwise we bail

        if (good_notes != active_notes) {
                // too few keys pressed; ignore.
                return false;
        }

        // if there are no notes active it can never be a reason for a reward here. 

        if (active_notes > 0) {

                // all the right notes are sounding
                // that '25' is the number of ms that a note can be paused for it to count as in time
                // TODO: ticks aren't really milliseconds, does this work as advertised?

                if ((state == running || (ticks_waiting < 25 || tickindex == 0)) && (!had_an_error)) {
                        // the notes were played faultless and on time, color them green as a very minor reward to the user

                        per_tick_stats_log(tickindex, { errors: 0, good: 1, delay: 0});

                        score.color_active_noteheads(tickindex, "green");
                }
                else {
                        // notes were good but too late

                        if (!had_an_error) {

                                per_tick_stats_log(tickindex, { errors: 0, good: 0, delay: 1});

				// the notes are already lightgreen at this point in time, so no need to color them
                        }
                }
        }

        // we re-enable the animation of the score

        if (state == waiting) {
                play_button();

                // play any grayed out notes to accompany the user

                play_grayed_notes(score.tickindex_to_x(tickindex));
        }

        // move to the next set of notes and restart the animation if it was paused
       
        // setting the tickindex here will rapidly advance it to the next
        // notes the user should play (for instance, if there are several
        // bars of grayed notes in between), but it should never lead
        // to advancing beyond the last tickindex
 
        set_tickindex(tickindex + 1);

        if (tickindex == last_tickindex) {
                // practice_stop();
        }

        // reset the sounding notes so they can be struck again
        // this leaves the notes_on_midi unchanged, those are
        // only used to display the notes_on cursors

        for (var i=0;i<n;i++) {
                notes_on[notes_sounding[i]] = 0;
        }

        return true;
}

// determine whether or not two intervals overlap anywhere
//
//
// x0...x1      s0...s1 <- out left
// x0...s0******s1...x1
// x0...s0******x1...s1
// s0...x0******x1...s1
// s0...s1......x0...x1 <- out right
// s0...x0******s1...x1

function overlaps(x0, x1, s0, s1)

{
        // out to the right
        if (x0 > s1) {
                return false;
        }

        // out to the left
        if (x1 < s0) {
                return false;
        }

        // in every other case there is overlap
        return true;
}

// move the score section to the left

function animate_score(scrolling)

{
        // also keep track of the scaling factor
        // TODO

        // this is what drives the animation:

        if (!scrolling) {
                score_xpos -= 2;
        }

        // Draw all canvases of which a part overlaps the screen at the right position
        // this ensures that even on slow machines or retina style displays the animation
        // is smooth. The idea here is to manipulate the absolute minimum amount of pixels.

        var x = score_xpos;

        for (canvas=0;canvas < score_canvases.length;canvas++) {

                // if part of the canvas is visible then animate it
                // this could be a little bit more optimal, that '0' could be the right hand side of the
                // clef-timesig-keysig portion
                // TODO

                // unless we are completely redrawing the score we only animate the canvases that
                // overlap the visible part of the screen. The reason why we update all of the canvases
                // during a redraw is to make sure that canvases that are 'off screen' get moved out of
                // the way.

                if (scrolling || overlaps(x, x + score_canvases[canvas].width, 0, window.innerWidth)) {

                        // console.log("animating ", canvas, " canvas ", score_canvases[canvas].canvas , " to x ", x);

			// document.getElementById(score_canvases[canvas].canvas).style.top = 8 + "px";
                        document.getElementById(score_canvases[canvas].canvas).style.left = x + "px";
                }
                x += score_canvases[canvas].width;
        }
        
        if (!scrolling) {
                if (config_mode == following || config_mode == automatic) {
                        // that -1 suppresses the gray notes for a single tick
                        // so poll_user gets a chance to pause the score if the
                        // user is late.

                        play_grayed_notes(Math.floor((hairline_x()/2))-1);
                }

                if (config_mode == listening && on_hotspot(score.tickindex_to_x(tickindex)*2)) {
                        // we are just listening to the piece, play both left and right hand side notes

                        play_active_notes(score.tickindex_to_x(tickindex), lefthand + righthand);

                        set_tickindex(tickindex+1);
                }

                if ((hairline_x() / 2) > (score.tickindex_to_x(last_tickindex)+2)) {
        
                        // we are at the end of the piece, stop the practice,
                        // stop the interval timer to shut down the animation 
                        // and re-enable the play button

                        // subtle bug here: this causes the *last* notes not to be
                        // detected because we are stopping practice when we stop
                        // the scrolling but the very last tick still needs to
                        // be detected

                        console.log("practice stop from animate_score");

                        practice_stop();
                }
        }
}

// rudimentary scroller.
// takes a tickindex and moves the score to that point
// also sets the tick index

function scroll_score_to(v)

{
        var ti = v;

        if (ti < 0) {
                ti = 0;
        }

        // if we are in follow mode afterwards the notes that we land on will be 
        // painted lightgreen

        if (ti > score.last_tickindex) {
                ti = score.last_tickindex;
        }

        var p = score.tickindex_to_x(ti);

        // move the score cursor to *just* in front of the
        // next notes to be played. 

        score_xpos = (740 - (p*2));

        animate_score(true);

        _set_tickindex(ti);
}

// compute the position of the hairline relative to the beginning of the piece
// this allows for precise interaction and playing back the notes at the right
// moment in time

function hairline_x()

{
        //                   L   R
        //       xpos        +-|-+ cursor
        //       |-------------|-----------------|
        //            0        |

        var cursor_rect = document.getElementById("hairline").getBoundingClientRect();

        return (cursor_rect.left - score_xpos) - 10;
}

function poll_user()

{
        // after a scroll we have some quiet time

        if (scrolling > 0) {
                scrolling--;
                return;
        }

        if (state == finished) {
                return;
        }

        // if we are following the user and they fell asleep then we 
        // need to pause the scrolling score until the user plays
        // the right keys and wakes up again. 

        // TODO there are *six* nested ifs here, that's 64 different combinations this needs to be simplified

        if (config_mode == following || config_mode == automatic) {

                if (state != paused) {

                if (!all_expected_notes_on()) {

                        if (state != paused) {
                                // we will wait for the user to catch up
                                // if we've reached the hotspot

                                if (on_hotspot((score.tickindex_to_x(tickindex))*2)) {

                                        // maybe we should disable the animation interval timer here?

                                        state = waiting;

                                        ticks_waiting = 0;

                                       // while we're paused indicate that we are waiting for some specific keys to be pressed
                                       // but if the notes are already read then we leave them that way.
   
                                       if (!had_an_error) {
                                               score.color_active_noteheads(tickindex, "lightgreen");
                                       }
                                }
                        }
                }
                }
        }
}

// one tick of the system clock

function one_tick()

{
        if (state == finished) {
                return;
        }

        if ((state == running || state == waiting) && ms_per_tick > 0) {
                total_practiced += ms_per_tick;
        }

        if (state == waiting) {
                ticks_waiting++;
                if (ms_per_tick > 0) {
                        total_waiting += ms_per_tick;
                }
        }
        else {
                ticks_waiting = 0;
        }

        // we first poll the user, this takes care of the edge case where
        // the poll results in a 'pause' otherwise we will end up playing
        // any gray notes twice, once when we pause and again when we
        // unpause and that does not sound nice.

        if (scrolling) {
                scrolling--;
                return;
        }

        if (state == running) {
                animate_score(false);

                poll_user();

                // attempt to reprogram the interval timer
                // this only succeeds whenever the hairline
                // cursor is exactly on top of new tickindex
                // maybe this should move to animate_score?
                // TODO
 
                reprogram_interval_timer();
        }
}

var interval_timer = null;

// if an interval timer is currently running 
// then get rid of it

function reset_interval_timer()

{
        // if this is not the first interval timer activated then reset the previous one

        if (interval_timer != null) {
                window.clearInterval(interval_timer);

                interval_timer = null;
        }
}

// the interval timer is free running, it drives
// the main animation and user polling tick, and
// because the timing can be off considerably 
// due to browser limitations we do a lot of 
// course correction to ensure we reach the right
// part of the score at the right moment in time.
//
// Because the user can get (considerably) ahead
// of the tempo we have to ensure we use the 
// actual position of the cursor rather than the
// tickindex (because it may already point to 
// notes much further down in the score)

function reprogram_interval_timer()

{
        // first we figure out where exactly the hairline cursor is
        // in the score
        
        var xpos = Math.floor((hairline_x()/2))-1;

        var ti = score.xpos_to_tickindex(xpos);

        // if we are not exactly on a tick then we will not 
        // recompute how fast to scroll
        // except for when no interval timer is active, in that
        // case we make sure that it is running even if that
        // is with the wrong value

        if (interval_timer != null && typeof(ti) == "undefined") {
                return;
        }

        // we are exactly on a tick

        // we need to go to the *next* tick so the interval and delta_t to use
        // are found by taking the next tick and subtracting from the current one

	console.log("recalculating interval timer for tick " , ti+1);

        ti = ti + 1;

        if (ti > last_tickindex) {
                return;
        }


        if (ti == 0) {
                        // special case the run-up to the first note
                delta_x = 50;

                delta_t = 250;
        }
        else {
                // all subsequent notes

                // we *are* at ti-1, we need to go to ti

                // the horizontal difference in pixels

                console.log("tickindex to x ", ti-1, score.tickindex_to_x(ti-1), ti, score.tickindex_to_x(ti));

                delta_x = (score.tickindex_to_x(ti) - score.tickindex_to_x(ti-1));

                // the time difference in milliseconds

                delta_t = score.interval(ti, ti-1);
        }

        if (delta_x == 0) {
                console.log("called reprogram_interval_timer no delta_x so no way to reprogram the timer!");

                if (interval_timer != null) {
                        // we were called with faulty parameters, the best possible
                        // thing we can do is absolutely nothing because there already
                        // is a timer running
                        return;
                }
        }

        reset_interval_timer();

        // compute how many pixels per second we need to scroll horizontally
        // to match the tempo of the piece

        // at two pixels of scrolling per tick how many milliseconds a tick is

        ms_per_tick = Math.floor(((delta_t / delta_x) / tempo_multiplier) + 0.5);	// force rounding

        console.log("ms_per_tick ", ms_per_tick, "delta_t", delta_t, "delta_x", delta_x, " multiplier ", tempo_multiplier);

        interval_timer = window.setInterval(one_tick, ms_per_tick);
}

function practice_stop()

{
        // we can only finish a practice once

        if (state == finished) {
                return;
        }

        console.log("in practice stop");

        if ((config_mode == following || config_mode == automatic) && score_filename != null) {

                message_user("End of the practice run; auto-rewinding in 2 seconds.");

                // the practice has ended, we are out of notes.

                console.log("end of the piece ", document.getElementById("vexsvg_00").style.left);

                console.log("errors per tick", per_tick_stats);

                end_time = Date.now()/1000;

                console.log("start_time: ", start_time, " end_time: ", end_time);

                console.log("waiting total:   ", total_waiting);
                console.log("practiced total: ", total_practiced);

                var should_take = total_practiced - total_waiting;

                console.log("should take: ", should_take);

                // use the cumulative delay of all notes and chords that were
                // slow rather than the overall playthrough time because you don't want someone
                // to be able to 'make up time', or scroll through a piece and upset the stats
                // TODO

                var speed = Math.floor(100 * (should_take / total_practiced));

                console.log(score_filename, start_tickindex, last_tickindex, total_errors, total_good, total_late, Math.floor(total_practiced/100)/10, speed);

                // if the user was practicing then save the practice session statistics

                practice_persist(score_filename, new Date(), start_tickindex, last_tickindex, total_errors, total_good, total_late, Math.floor(total_practiced/100)/10, speed, config_hand);

                practice_log_retrieve(score_filename, start_tickindex, last_tickindex, config_hand);

                // make sure the stats are saved 

                score_save();

                per_tick_stats_compute_scores(score, per_tick_stats);

                window.setTimeout(function () {
			if (config_mode == automatic) {
				autopilot_practice_range(score, per_tick_stats);
			}
			else {
                        	rewind();
			}
                }, 2000);                
        }

        reset_interval_timer();

        state = finished;
}

// move to a new tickindex, effectively moving to a new set of notes
// because the tempo can change from one tickindex to the next and
// because ticks are variable width we need to reprogram the interval
// timer here to ensure the score scroll speed is correct

function _set_tickindex(ti)

{
        if (ti != tickindex) {

                tickindex = ti;

                // reset the 'had an error' flag, presumably we are going to 
                // move to a new set of notes now so there is no error in them
                // so far

                had_an_error = false;
        }

        // we are on the move again; regardless of how we got here
        // we enable the animation to continue play unless we are at
        // the end of the piece.

        if (state == waiting) {
                play_button();
        }
}

// to avoid creating a loop where a change to the scrollbar
// causes the scrollbar to be animated we call a hidden
// version of set_tickindex here and use that when we
// manipulate the scrollbar. The hidden one does not update
// the scrollbar position.

function set_tickindex(ti)

{
        _set_tickindex(ti);

        animate_scrollbar(tickindex);
}

function blank_score_stat()

{
        var s = [];

        for (var i=0;i<(score.last_tickindex+1);i++) {
                // this ... bullshit makes a deep copy of the array in blank_per_tick_stat
                // apparently copying by reference is the default for arrays in JS
                s[i] =  [... blank_per_tick_stat];
        }

        return s;
}

// 

function load_score(filename)

{
        var row = retrieve_score(filename);

        if (row == null) {
                // TODO
                // we may be able to get rid of this entirely 
                // now that the per_tick_stats array is expanded dynamically
        
                per_tick_stats = [];
        
                // per_tick_stats = blank_score_stat();
        }
        else {
                console.log(row);
        }
}

function populate_file_select()

{
        selectfile.innerHTML = pieces.map(piece => '<option value="' + piece + '">' + piece + '</option>').join('');

        // show which file is currently active 
        // (How does this work if there are no files in the dropdown)
        // TODO

        $('#selectfile option[value="' + score_filename + '"]').prop('selected', 'selected');
}

var redraw_filename = '';
var redraw_hand = -1;
var redraw_diff = -1;
var redraw_klow = -1;
var redraw_khigh = -1;
var redraw_start = -1;
var redraw_last = -1;
var redraw_force = false;

// figure out if enough has changed since the last time we were here that we need to redraw the score

function must_redraw(score_filename, config_hand, klow, khigh, start_tickindex, last_tickindex)

{
	var r = false;

        if (redraw_force) {
                r = true;
                redraw_force = false;
        }

	if (score_filename != redraw_filename) {
		r = true;
	}
	if (config_hand != redraw_hand) {
		r = true;
	}
        if (config_diff != redraw_diff) {
                r = true;
        }
	if (klow != redraw_klow) {
		r = true;
	}
	if (khigh != redraw_khigh) {
		r = true;
	}
	if (start_tickindex != redraw_start) {
		r = true;
	}
	if (last_tickindex != redraw_last) {
		r = true;
	}

	redraw_filename = score_filename;
	redraw_hand = config_hand;
        redraw_diff = config_diff;
	redraw_klow = klow;
	redraw_khigh = khigh;
	redraw_start = start_tickindex;
	redraw_last = last_tickindex;

	return r;
}

function redraw()

{
	if (score == null || must_redraw(score_filename, config_hand, kbd_midi_low, kbd_midi_high, start_tickindex, last_tickindex)) {
        	score = new Score(score_midi, score_canvases, canvas_width, config_diff);
	
        	// timesig, keysig and scale should be supplied by score, not some global
        	// TODO

        	draw_clefs_keysig_timesig(keysig, scale, timesig);

        	draw_filler();
	}
        // now we know how many tickables there really are in the
        // score so we can set the range of the sliders

        // but only if the sliders have initialized (otherwise it doesn't
        // matter anyway...)

        if($("#scroller").slider("instance")) {
                $("#scroller").slider("option", "max", score.last_tickindex);
                $("#slider-range").slider("option", "max", score.last_tickindex);
		$("#slider-range").slider("values", [start_tickindex, last_tickindex]);

        }

        per_tick_stats_compute_scores(score, per_tick_stats);
}

var rewinding = false;

function rewind()

{
        if (rewinding) {
                return;
        }

        rewinding = true;

        practice_persisted = false;

	// this redraw on every rewind is much too time consuming.
	// TODO

        redraw();

        // move the animation to the leftmost point.

        tickindex = -1;

        // a rewind is a good moment to save the score and the associated statistics
        // we probably should do this at the end of a piece too, in case rewind is
        // never triggered

        // we probably should save the lesson plan and first and last bar as well so
        // we can pick up again where we left off later.

        if (per_tick_stats == null) {
                // new file, blank the score stats

                per_tick_stats = [];

                // per_tick_stats = blank_score_stat();
        }

        score_save();

        stats_counters_reset();

        // show the results of similar practices in the past

        practice_log_retrieve(score_filename, start_tickindex, last_tickindex, config_hand);

        rewinding = false;

        animate_scrollbar(start_tickindex);

        scroll_score_to(start_tickindex);

        draw_all_tick_stats();

        // after a rewind the user will have to press 'play'. This is 
        // probably not optimal in many cases
        // TODO

	if (config_mode == automatic) {
		state = running;
	}
	else {
        	state = paused;
	}
}

// callback function that is activated when a score load from
// the database or from a midi file has been completed. 

function score_loaded()

{
        // console.log("midi score: ", score_midi);

        // we need to save the configuration because the filename of the
        // score we are playing has changed. 

        config_save();

	rewind();

	autopilot_init(score, per_tick_stats);

        state = paused;

        // reflect the active score in the pieces drop=down menu

        // restore the UI range slider after a score is loaded
        // first compute range slider position

        // TODO
        // verify if rewind doesn't already take care of this

        if ($("#slider-range").slider("instance")) {
                $("#slider-range").slider("values", [start_tickindex, last_tickindex]);
        }

        populate_file_select();

        // this should reset the stats displyed we can get rid of it when we implement some other mechanics of displying progress including historical data

        show_stats_counters();

        // dirty solution to update UI with the title
        // TODO: get rid of it when a new file name select is created
        update_score_title(score_filename);


}

// TODO verify if this works properly with all keys

function vf_key_spec(keysig, scale) 

{
        if (scale == 'minor') {
                keysig = keysig + "m";
        }

        return keysig;
}

// this is a separate div, it is stationary so the one with the notes can scroll
// and the clefs and key signature remain visible.

function draw_clefs_keysig_timesig(keysig, scale, timesig)

{

        $(".clefsandsig").empty();

        // console.log(keysig, scale, timesig)

        keyspec = vf_key_spec(keysig, scale);

        var csrenderer = new VF.Renderer(document.getElementById("clefsandsig") , VF.Renderer.Backends.SVG);

        csrenderer.resize(320, 600);
        var cscontext = csrenderer.getContext();

        // fill the background of the clefs and key signature section with white,
        // that way it will eat up the notes that scroll in from the notes section
        // as they move under it.

        cscontext.setBackgroundFillStyle(config_paper);
        cscontext.clearRect(0,0,320,600);

        cscontext.scale(2,2);

        // TODO bpm is not visible; in general tempo indication gets cut off

        var cs_treble_stave = new VF.Stave(10, 60, 200).addClef('treble').addTimeSignature(timesig).addKeySignature(keyspec);
        var cs_bass_stave = new VF.Stave(10,160, 200).addClef('bass').addTimeSignature(timesig).addKeySignature(keyspec);

        // Connect it to the rendering context and draw!
        cs_treble_stave.setContext(cscontext).draw();
        cs_bass_stave.setContext(cscontext).draw();

        var connector = new VF.StaveConnector(cs_treble_stave, cs_bass_stave);

        var lineRight = new VF.StaveConnector(cs_treble_stave, cs_bass_stave).setType(0);
        var lineLeft = new VF.StaveConnector(cs_treble_stave, cs_bass_stave).setType(1);

        connector.setContext(cscontext).draw();
        lineRight.setContext(cscontext).draw();
        lineLeft.setContext(cscontext).draw();
}

// the stationary filler between the clefs and the key signature
// and the actual score. The score will later scroll over the filler
// all it does is to create a visual connection between the clefs and
// key signature and the first notes of the score at the beginning of
// a piece.

function draw_filler()

{
        var w = 2000;   // wide enough to clear the right side of the window

        $(".filler").empty();

        var frenderer = new VF.Renderer(document.getElementById("filler") , VF.Renderer.Backends.SVG);

        frenderer.resize(w, 600);
        var fcontext = frenderer.getContext();

        fcontext.setBackgroundFillStyle(config_paper);
        fcontext.clearRect(0,0,w,600);

        fcontext.scale(2,2);

        var f_treble_stave = new VF.Stave(10, 60, w);
        var f_bass_stave = new VF.Stave(10,160, w);

        // Connect it to the rendering context and draw!
        f_treble_stave.setContext(fcontext).draw();
        f_bass_stave.setContext(fcontext).draw();

}

// check if we already have this file

function already_in_pieces(filename)

{
        for (var i=0;i<pieces.length;i++) {
                if (pieces[i] == filename) {
                        return true;
                }
        }
        return false;
}

function only_unique(value, index, self) {
        return self.indexOf(value) === index;
}

function score_add(filename, midi)

{
        if (already_in_pieces(filename)) {
                // this is a score we already have

                console.log("already have this piece");

                score_update_midi(filename, midi);
        }
        else {
                // new score
                score_midi = midi;
                score_filename = filename;
                per_tick_stats = [];

                // get rid of old history for this filename if we already had it
                // we don't actually want this; if there already is a set of stats
                // then we should retain them so a small update of a midi file does
                // not immediately result in the user losing all their work. 
                // score_delete(filename);

                // maximize the range slider

                start_tickindex = 0;
                last_tickindex = 1000000;

                score_loaded();
        
                pieces.push(filename);

                pieces = pieces.filter(only_unique);

                populate_file_select();
        }
}

function parse_midi(filename, buf) 

{
        const midi = new Midi(buf);

        // use this to add files to the built-in repertoire

        // console.log("midi start:");

        // console.log(JSON.stringify(midi));

        // console.log("midi end...");

        score_add(filename, midi);
}

function play_button()

{
        state = running;

        if (tickindex < last_tickindex) {

                // if we start playing right on top of grayed
                // out notes make them sound

                if (config_mode == following || config_mode == automatic) {
                        play_grayed_notes(Math.floor((hairline_x()/2)-1));
                }
        }

        reprogram_interval_timer();
}

function pause_button()

{
        state = paused;

        // make sure the stats are saved 

        score_save();

        midi_notes_off();
}

function enable(id)

{
        $(id).attr("disabled", false);
}

function disable(id) 

{
        $(id).attr("disabled", true);
}

// this gets called once per second to ensure the UI is 
// in a usable state, it allows state changes to be 
// done in background tasks without having to update
// the ui all the time. 

function controls_check()

{
        // console.log("state is ", state, " interval_time active ", interval_timer != null);

        switch (state) {
                case finished:
                        disable("#pausebutton");
                        disable("#playbutton");
                        break;
                case running:

                        console.assert(interval_timer != null);

                        enable("#pausebutton");
                        disable("#playbutton");
                        break;
                case paused:
                        disable("#pausebutton");
                        enable("#playbutton");
                        break;
                case waiting:
                        disable("#pausebutton");
                        disable("#playbutton");
                        break;
        }

        if (tickindex > 0 || state == waiting) {
                enable("#rewind");
        }
        else {
                disable("#rewind");
        }
}

// set up the file reader for the user to choose midi files locally

function read_file(e) {
        const file = e.target.files[0]
        let reader = new FileReader();

        console.log("new filereader active");

        reader.onload = function(e) {

                // persist the stats of the previous file before overwriting them

                score_save();

                console.log("reader result: ", reader.result);

                let arrayBuffer = new Uint8Array(reader.result);
                console.log(arrayBuffer);
                parse_midi(file.name, arrayBuffer);

                message_user("loaded file " + file.name);
          }

          reader.readAsArrayBuffer(file);
}

function add_file(e) {
    document.querySelector("#fileItem").click()
}

function del_file(e) {
        // get the currently selected file
        const selectFile = document.getElementById("selectfile");
        const currentIndex = selectFile.selectedIndex;
        const currentPiece = pieces[currentIndex];

        if (!confirm("Click OK to confirm you want to delete " + currentPiece)) {
                console.log('aborting');
                return;
        }
        console.log('deleting', currentPiece);
        // we need to figure out what the new selected piece will be
        // nice to simply move down in the list unless it was the last element
        // if it was the last element, then it will be the new array's length-1 in index
        const nextIndex = Math.min(currentIndex, selectFile.length-2);
        const nextFile = pieces[nextIndex];

        // remove it from IndexDB
        // because lovefield's delete doesn't guarantee deleting the object, we can instead get the ID
        // and delete it outselves directly from IndexDB
        const scores = database.getSchema().table('Scores');
        database.delete().from(scores).where(scores.filename.eq(currentPiece)).exec().then(function
                (results) {
                console.log("deleted", currentPiece);
                pieces.splice(currentIndex, 1);
                // and now refresh the dropdown
                populate_file_select();
                score_retrieve(nextFile);
                selectFile.selectedIndex = nextIndex;
        });
}

function start_tone_js()

{
        if (synth != null) {
                return;
        }

        if (config_midiout != 0) {
                return;
        }

        // create a sampler synth, reusing the salamander piano
        // files from Tone js. in the future will be good to find
        // the full set and host ourselves.
        synth = new Tone.Sampler({
                urls: {
                    "A0": "A0.mp3",
                    "A1": "A1.mp3",
                    "A2": "A2.mp3",
                    "A3": "A3.mp3",
                    "A4": "A4.mp3",
                    "A5": "A5.mp3",
                    "A6": "A6.mp3",
                    "A7": "A7.mp3",
                    "C1": "C1.mp3",
                    "C2": "C2.mp3",
                    "C3": "C3.mp3",
                    "C4": "C4.mp3",
                    "C5": "C5.mp3",
                    "C6": "C6.mp3",
                    "C7": "C7.mp3",
                    "C8": "C8.mp3",
                    "D#1": "Ds1.mp3",
                    "D#2": "Ds2.mp3",
                    "D#3": "Ds3.mp3",
                    "D#4": "Ds4.mp3",
                    "D#5": "Ds5.mp3",
                    "D#6": "Ds6.mp3",
                    "D#7": "Ds7.mp3",
                    "F#1": "Fs1.mp3",
                    "F#2": "Fs2.mp3",
                    "F#3": "Fs3.mp3",
                    "F#4": "Fs4.mp3",
                    "F#5": "Fs5.mp3",
                    "F#6": "Fs6.mp3",
                    "F#7": "Fs7.mp3",                
                },
                release: 1,
                baseUrl:  "https://tonejs.github.io/audio/salamander/"
        }).toDestination();
}

document.querySelector("#fileItem").onchange=read_file
document.querySelector("#addmidi").onclick=add_file
document.querySelector("#delmidi").onclick=del_file

// rewind button

document.getElementById('rewind').addEventListener('click', (e) => {
        rewind();
})

// pause / play button

document.getElementById('playbutton').addEventListener('click', (e) => {
        play_button();
});

document.getElementById('pausebutton').addEventListener('click', (e) => {
        pause_button();
});

$( function() {
	$("#scroller").slider({
           	min: 0,
           	max: 1000,
	    	slide: function(e, ui) {
		    	scrolled(ui.value);
	    	},
	    	stop: function(e, ui) {
		    	scrolled(ui.value);
		}
        });
} );

$( function() {
	$("#speed").slider({
        	min: 50,
           	max: 200,
	   	value: 100,
           	change: speedchange,
     	});
} );

var rs_start_pos = 0;
var rs_end_pos = 1000;
var rs_delta = 1000;

$( function() {
	$( "#slider-range" ).slider({
	    range: true,
	    min: 0,
	    max: 1000,
	    values: [ 0, 1000 ],
		// the start function is used to determine how we began the slide
		// so if possible the distance can be kept constant
		start: function( event, ui ) {
			rs_start_pos = ui.values[0];
			rs_end_pos = ui.values[1];
			rs_delta = rs_end_pos - rs_start_pos;
		},
		slide: function range_slide_func( event, ui ) {
			console.log(ui.values[0], ui.values[1]);
			if (ui.values[0] != rs_start_pos) {
				// we are moving the left hand slider handle, keep the
				// right hand one at the same distance as it was at the
				// start of this slide operation

				$("#slider-range").slider("values", [ui.values[0], ui.values[0] + rs_delta]);
				practice_range_start(ui.values[0], ui.values[0] + rs_delta);
			}
			else {
				// we are moving the right hand slider handle

                        	practice_range_last(ui.values[0], ui.values[1]);
			}
		},
		stop: function( event, ui) {
			if (ui.values[0] != rs_start_pos) {
				$("#slider-range").slider("values", [ui.values[0], ui.values[0] + rs_delta]);
				practice_range_start(ui.values[0], ui.values[0] + rs_delta);
			}
			scrolled(ui.values[0]);
			rewind();
			// if we have practices this stretch before then retrieve the session logs
                        practice_log_retrieve(score_filename, start_tickindex, last_tickindex, config_hand);
		}
	});

});

function animate_scrollbar(v)

{
        // ensure the slider is initialized or this will throw an error.
        if($("#scroller").slider("instance")) {
                $("#scroller").slider("value", v);
        }
}

function scrolled(v)

{
        scrolling = 50;         // suppress sound for the next n ticks to ensure we don't go mad from sound during scrolling

        // console.log("scroller value: ", v);

        scroll_score_to(v);

        if (state == finished) {
                state = paused;
        }
}

function speedchange(e)

{
        tempo_multiplier = $("#speed").slider("value") / 100;
}

function set_keyboardrange(l,h)

{
        kbd_midi_low = l;
        kbd_midi_high = h;

        config_save();

        redraw();
}

function set_keyboard_size(v)

{
        // TODO check all these with actual devices

        switch (Math.floor(v)) {        // nasty: that value from the select is a string and switch does not recognize that you are comparing strings and ints
                                        // I hate untyped languages, especially when under the hood they *do* have types!
                case 25:
                        set_keyboardrange(48,72);
                        break;
                case 32:
                        set_keyboardrange(53,84);
                        break;
                case 37:
                        set_keyboardrange(48,84);
                        break;
                case 49:
                        set_keyboardrange(36,84);
                        break;
                case 61:
                        set_keyboardrange(36,96);
                        break;
                case 73:
                        set_keyboardrange(28,100);
                        break;
                case 76:
                        set_keyboardrange(28,103);
                        break;
                case 85:
                        set_keyboardrange(21,105);
                        break;
                case 88:
                        set_keyboardrange(21,108);
                        break;
                default: 
                        console.log("unrecognized: ", v);
        }
}

function keys_change(e)

{
        set_keyboard_size($("#selectKeys").val());

        config_save();

        message_user("Changed keyboard size to " + $("#selectKeys").val() + " keys.");
}

function hand_change()

{
        config_hand = $("#hand").val();

        config_save();

        redraw();

        practice_log_retrieve(score_filename, start_tickindex, last_tickindex, config_hand);

        message_user("Practicing " + hand_desc(config_hand));

        draw_all_tick_stats();
}

function score_load(filename, midi, stats, start, last)

{
        // console.log("loading ", filename);
       
        score_filename = filename; 
        score_midi = midi;
        per_tick_stats = stats;

        // these checks for undefined take care of old
        // database records, at some point they can be
        // dropped.

        if (typeof(start) != "undefined") {
                // console.log("in score_load start ", start, " last " , last);

                start_tickindex = start;
        }
        else {
                // console.log("setting start_tickindex to 0 in score_load");

                start_tickindex = 0;
        }

        if (typeof(last) != "undefined") {
                last_tickindex = last;
        }
        else {
                last_tickindex = 1000000;
        }



        score_loaded();
}

function file_change()

{
        // persist the stats of the previous file

        score_save();

        var piece = pieces[ document.getElementById("selectfile").selectedIndex];

        score_retrieve(piece);
}

function practice_range_start(first, last)

{
        if (first != start_tickindex) {
                scroll_score_to(first);
                start_tickindex = first;
		draw_all_tick_stats();
        }
        last_tickindex = last;

        state = paused;

        scrolling = 50;
}

function practice_range_last(first, last)

{
        start_tickindex = first;

        if (last != last_tickindex) {
                scroll_score_to(last);
                last_tickindex = last;
		draw_all_tick_stats();
        }

        state = paused;

        scrolling = 50;
}

function db_now_connected()

{
        db_connected = true;

        config_retrieve();

        pieces_retrieve();
}

// we use a localstorage item in order to determine if the user
// has already ran the application before. If so then we
// will not attempt to re-initialize the repertoire

function is_firstrun()

{
        if (localStorage.getItem("alreadyran") != "1") {
                return true;
        }

        return false;
}

var initialized_repertoire = false;
var bootstrap_timer = null;

// this routine gets called once every second if this is the
// first run of the application. Once the database is 
// connected we use it to pre-populate the repertoire with
// some pieces to play

function bootstrap_check()

{
        // the DB needs to be alive for this to work

        if (!db_connected) {
                return;
        }

        // and we only do it once

        if (initialized_repertoire) {
                return;
        }

        // ensure the timer stops calling us

        window.clearInterval(bootstrap_timer);

        bootstrap_timer = null;

        // populate the db with some new pieces

        for (var i=0;i<repertoire.length;i++) {
                // console.log(repertoire[i]);
                score_add(repertoire[i].filename, JSON.parse(repertoire[i].midi));
        }

        // and set the cookie so we won't come back here again

        localStorage.setItem("alreadyran", "1");

        initialized_repertoire = true;
}

// This captures any clicks anywhere in the doc. The idea
// here is that it will help to get Tone.js started if
// that is out preferred output method without an explicit
// Tone.js start button

function any_click()

{
        start_tone_js();
}

// Rudimentary solution to update piece title for UI element 
// without .mid file name part. 

function update_score_title(title) 

{
        $("#pieceTitle").text(title.replace(".mid", ""));
      //  $("#pieceTitle").append("<div class='arrow down'></div>");
}

// Set of functions to show and hide settings menu

function toggleNav() {
        navSize = document.getElementById("topmenuwrapper").style.height;
        if (navSize == "310px") {
                return closeNav();
        }
        return openNav();
}

function openNav() {
        document.getElementById("topmenuwrapper").style.height = "310px";
        document.getElementById("insideBar").style.height = "310px";
        document.getElementById("topmenuwrapper").style.width = "100%";
}

function closeNav() {
        document.getElementById("topmenuwrapper").style.height = "80px";
        document.getElementById("insideBar").style.height = "80px";
}

document.body.addEventListener('click', any_click, true);

// main program starts here

VF = Vex.Flow;

show_notes_on();

rewind();

if (is_firstrun()) {
        bootstrap_timer = window.setInterval(bootstrap_check, 1000);
}

// periodically check the state of the program and
// adapt the UI accordingly

window.setInterval(controls_check, 500);


// keyboard bindings to help with navigation
// space plays or stops the piece, left arrow rewinds.
$("body").on("keydown", function(e){
        if(e.which == 32 && $("#playbutton").is(":disabled")) $("#pausebutton").click();
        if(e.which == 32 && $("#pausebutton").is(":disabled")) $("#playbutton").click();
        if(e.which == 37 && $("#rewind").is(":enabled")) $("#rewind").click();
});


// chrome only wakelock to prevent screansaver during practice 
// TODO: wakelock should only be called at the start of the practice and released once the practice is paused. 
// It'll release itself onse the tab becomes invisible so we need to also add window.onfocus handling.
if ('wakeLock' in navigator) {
        navigator.wakeLock.request('screen');
}


