//
// JavaScript Music Trainer
// (C) Jacques Mattheij 2020; jacques@modularcompany.com
// All rights reserved, 
//
// This source file deals with rendering the json encoded
// midi score onto a canvas as well as creating a lot of 
// useful meta data about the score as rendered, which is
// kept around and made accessible through a number of 
// public functions
//

class Score {

        // TODO
        // test key_to_midi exhaustively with unit tests

        key_to_midi(key, acc)

        {
                var pitchoffets = [0, 2, 3-12, 5-12, 7-12, 8-12, 10-12];

                var note = 21;

                // A or a = 0
                // B or b = 2
                // etc
                // G or g = 10

                var pitch = pitchoffets[key.key.toUpperCase().charCodeAt(0) - 65];
                var octave = key.octave;

                note += 12 * octave;

                if (acc == '#') {
                        note++;
                }

                if (acc == 'b') {
                        note--;
                }

                note += pitch;

                return note;
        }

        // There really are only 15 different possibilities, ranging from
        // 7 sharps to 7 flats and everything in between.
        //
        // But key signature names can overlap, there are 2x 15 key signature
        // names each of which can map onto their corresponding minor or major
        // mirror image with the exact same number of sharps and flats. So
        // we only need 15 lookup tables to be able to process all keys.

        // TODO test this very carefully for all notes and key signatures

        // TODO add natural signs for keys that have sharps and flats in them
        // so the natural notes are properly displayed

        keysig_to_lut(keysig)

        {
                switch (keysig) {
                        // no sharps or flats
                        default:
                        case 'C':
                        case 'Am':
                                return "C C#D EbE F F#G G#A BbB ";
                        // one sharp (F#)
                        case 'G':
                        case 'Em':
                                return "C C#D D#E FnF#G G#A BbB ";
                        // two sharps (F#, C#)
                        case 'D':
                        case 'Bm':
                                return "CnC#D D#E FnF#G G#A A#B ";
                        // three sharps (F#, C#, G#)

                        case 'A':
                        case 'F#m':
                                return "CnC#D D#E FnF#GnG#A A#B ";

                        // four sharps (C#, F#, G#, D#)
                        case 'E':
                        case 'C#m':
                                return "CnC#DnD#E FnF#GnG#A A#B ";
                        // five sharps (D#, C#, F#, G#, A#)

                        case 'B':
                        case 'G#m':
                                return "CnC#DnD#E FnF#GnG#AnA#B ";
                        // six sharps (D#, C#, F#, G#, A#, D#)
                        case 'F#':
                        case 'D#m':
                                return "CnC#DnD#EnFnF#GnG#AnA#B ";
                        // seven sharps
                        case 'C#':
                        case 'A#m':
                                return "CnC#DnD#EnFnF#GnG#AnA#Bn";

                        // seven flats
                        case 'Cb':
                        case 'Abm':
                                return "CnDbDnEbFbFnGbGnAbAnBbCb";

                        // six flats
                        case 'Gb':
                        case 'Ebm':
                                return "CnDbDnEbEnF GbGnAbAnBbCb";
                        // five flats
                        case 'Db':
                        case 'Bbm':
                                return "C DbDnEbEnF GbGnAbAnBbBn";
                        // four flats
                        case 'Ab':
                        case 'Fm':
                                return "C DbDnEbEnF GbG AbAnBbBn";
                        // three flats
                        case 'Eb':
                        case 'Cm':
                                return "C DbD EbEnF GbG AbAnBbBn";
                        // two flats
                        case 'Bb':
                        case 'Gm':
                                return "C DbD EbEnF GbG AbA BbBn";
                        // one flat
                        case 'F':
                        case 'Dm':
                                return "C DbD EbE F GbG AbA BbBn";
                }
        }

        // the initial state of the accents for each note
        // this is used at the beginning of each bar to 
        // reset the accents to what the key the piece is
        // currently prescribes.

        keysig_to_bar_initial(keysig)

        {
                switch (keysig) {
                        // no sharps or flats
                        default:
                        case 'C':
                        case 'Am': //    A   B   C   D   E   F   G
                                return ['n','n','n','n','n','n','n'];
                        // one sharp (F#)
                        case 'G':
                        case 'Em':
                                return ['n','n','n','n','n','#','n'];
                        // two sharps (F#, C#)
                        case 'D':
                        case 'Bm':
                                return ['n','n','#','n','n','#','n'];
                        // three sharps (F#, C#, G#)

                        case 'A':
                        case 'F#m':
                                return ['n','n','#','n','n','#','#'];

                        // four sharps (C#, F#, G#, D#)
                        case 'E':
                        case 'C#m':
                                return ['n','#','#','n','n','#','#'];

                        // five sharps (D#, C#, F#, G#, A#)

                        case 'B':
                        case 'G#m':
                                return ['#','#','#','n','n','#','#'];
                        // six sharps (D#, C#, F#, G#, A#, D#)
                        case 'F#':
                        case 'D#m':
                                return ['#','#','#','#','n','#','#'];
                        // seven sharps
                        case 'C#':
                        case 'A#m':
                                return ['#','#','#','#','#','#','#'];

                        // seven flats
                        case 'Cb':
                                return ['b','b','b','b','b','b','b'];

                        // six flats
                        case 'Gb':
                        case 'Ebm':
                                return ['b','b','b','b','b','n','b'];
                        // five flats
                        case 'Db':
                        case 'Bbm':
                                return ['b','b','n','b','b','n','b'];
                        // four flats
                        case 'Ab':
                        case 'Fm':
                                return ['b','b','n','b','b','n','n'];
                        // three flats
                        case 'Eb':
                        case 'Cm':
                                return ['b','b','n','n','b','n','n'];
                        // two flats
                        case 'Bb':
                        case 'Gm':
                                return ['n','b','n','n','b','n','n'];
                        // one flat
                        case 'F':
                        case 'Dm':
                                return ['n','b','n','n','n','n','n'];
                }

        }

        // This converts a midi pitch and a key signature into
        // a one or two character pitch descriptor + an octave. 

        // Note that this is far from perfect, more context is
        // required to reliably distinguish between for instance
        // C# or Db. One way would be to look at the chord (if
        // any) the note is played in, another would be to look
        // at data from previous (and possibly subsequent) notes
        // indicating whether we are in an ascending or descending
        // part of the melody line.

        midi_to_key(keysig, accents, next_accents, midi)

        {
                var lut = this.keysig_to_lut(keysig);

                // at which index to retrieve they key+accent(if any)

                var index = (midi % 12) * 2;

                var octave = Math.floor((midi - 12) / 12);

                // get the key+accent; strip out any whitespace

                var key = lut.substr(index, 2).replace(/\s+/g, '');

                // fish out what if any accent is attached to this key

                var keyno = key.charCodeAt(0) - 65;

                // if this note is spelled as a natural but a preceding
                // note of this kind was a flat or a sharp then ensure
                // we will output a natural sign

                // so for instance Gb G will become Gb Gn

                var accent;

                if (key.length == 1) {
                        if (accents[keyno] != 'n') {
                                key = key + 'n';
                        }
                        accent = 'n';
                }
                else {
                        accent = key.substr(1,1);
                }

                // if there was any change then remember it here, that ensures that for the next
                // tick we will use the updated accent set, but *within* this tick we use the previous
                // one, so that Gb3, (G3 G4) will come out as Gb3, (Gn3, Gn4). Otherwise
                // only one of the two G's in the chord would get a neutral sign.

                next_accents[keyno] = accent;

                return key + octave;
        }

/*
console.log("A0 comes back as ", midi_to_key("C", 21));

console.log("C4 comes back as ", midi_to_key("C", 60));


console.log("key A0 ", key_to_midi({ key: "A", octave: 0 }, null));
console.log("key B0 ", key_to_midi({ key: "B", octave: 0 }, null));
console.log("key C1 ", key_to_midi({ key: "C", octave: 1 }, null));
console.log("key D1 ", key_to_midi({ key: "D", octave: 1 }, null));
console.log("key E1 ", key_to_midi({ key: "E", octave: 1 }, null));
console.log("key F1 ", key_to_midi({ key: "F", octave: 1 }, null));
console.log("key G1 ", key_to_midi({ key: "G", octave: 1 }, null));
console.log("key A1 ", key_to_midi({ key: "A", octave: 1 }, null));
console.log("key B1 ", key_to_midi({ key: "B", octave: 1 }, null));

console.log("key C2 ", key_to_midi({ key: "C", octave: 2 }, null));

console.log("key C4 ", key_to_midi({ key: "C", octave: 4 }, null));
console.log("key A0 ", key_to_midi({ key: "A", octave: 0 }, null));
console.log("key C8 ", key_to_midi({ key: "C", octave: 8 }, null));
*/

        // append a single key to the list of keys expected
        // for this x position keys should remain sorted during this operation

        expect_key(bar, ti, rank, xpos, voice, key, accidental, duration_ms, volume, notehead)

        {
                if (duration_ms > 0) {
                }
                else {
                        // this should never ever happen.

                        console.log("key " , key, " duration_ms " , duration_ms , " volume " , volume);

                        console.log("notehead.duration " , notehead.duration);

                        debugger;
                }

                var midi = this.key_to_midi(key, accidental);

                // determine whether or not this notehead should be grayed out

                var grayed = false;

                // if the notehead is out of the practice area make it gray

                if (ti < start_tickindex || ti > last_tickindex) {
                        grayed = true;
                }

                // figure out if this note is in the range the users' keyboard can play

                if (midi < kbd_midi_low) {
                        grayed = true;
                }

                if (midi > kbd_midi_high) {
                        grayed = true;
                }

                // if the rank is greater than or equal to the difficulty 
                // selected then this note should not be played by the 
                // user but by the computer.

                if (rank >= this.diff) {
                        grayed = true;
                } 

                // figure out if this note is in a hand that we are practicing

		var hand;

		if (voice == 0) {
			hand = righthand;
		}
		else {
			hand = lefthand;
		}

                if (config_hand != bothhands) {
                        if ((config_hand == lefthand && voice == 0) || (config_hand == righthand && voice == 1)) {
                                grayed = true;
                        }
                }

                if (grayed) {
                        this.color_notehead(notehead, "gray");
                }

                // console.log("xpos: ", xpos);
                // console.log("len   ", this.notes.length);

		// save tickindex and bar to help autopilot.

                for (var i=0;i<this.notes[xpos].length;i++) {
                        if (this.notes[xpos][i].midi == midi) {
                                // somehow we have a double note!

                                console.log("double note in midi file at bar ", bar, "hand", hand, "midi", midi);

                                return;
                        }
                }

                this.notes[xpos] = this.notes[xpos].concat({ bar: bar, hand: hand, tickindex: ti, midi: midi, voice: voice, duration: duration_ms, volume: volume, grayed: grayed, notehead: notehead});
        
                // we know the note should be played at this point, but after it is down it
                // may overlap with subsequent tickables and there the note can be safely
                // ignored even if it is played. This would allow a user to 'fix' a mistake
                // by for instance playing the same broken chord again, even though the
                // previous notes were still sounding rather than that they would be played
                // again. Otherwise it becomes quite hard to recover from certain mistakes
                // for instance this 'rolled' chord:
                //
                // C------------
                //     E--------
                //         G----
                //
                // When playing the 'G' wrong releasing all notes and trying again to build
                // up the chord would result in an error because the C and E have already
                // passed. The CEG chord then needs to be released and *only* the G should
                // be played. This is confusing to the user because in one circumstance it
                // is entirely ok to have CEG and the G gets counted as 'Ok', but in another
                // the CEG G gets seens wrong because CE is present as well. The physical
                // situation at the keyboard is even more confusing because once C and E
                // have been released there is no way to recreate the 'proper' situation with
                // three keys depressed to get the "G" accepted as good. You can only get
                // it accepted by *releasing* the C and the E. 
                //
                // So we need some way of passing on the fact that C and E *can* be played
                // while playing G. This leads to another class of mistakes (double strikes)
                // to be ignored; for instance
                //
                // C--C---------
                //    E---------
                //
                // would no longer register as an error in that scenario. Still, I think that
                // allowing the user to get away with an error like that is preferable to 
                // blocking further progress until all keys are released and only the last
                // note is replayed, that completely breaks the flow of playing/studying.
                // An alternative is to count the note as 'wrong' anyway but to allow the
                // fix, that way the error count will be correct but we can keep moving.

                // So, to fix this we effectively have keys that *have* to be played in a
                // particular tick and keys that *may* be played in that tick (because they
                // are still down from before). 
                //
        
                // A pianoroll like construct would be the easiest way to determine
                // this. 128 midi pitches over a few thousand tickables at most would
                // be enough to store that information, which is a small price to pay.

                // one reasonably compact structure would use a string of hex characters
                // four bits per character. 128 bits per tick is then 32 bytes, 
                // at millisecond resolution that would give 32K / second. A five minute
                // piece (300 seconds) then gives about a megabyte. But we don't actually
                // need a resolution that high, 50 ticks / second would be plenty.

                // console.log("tick ", ti, " midi ", midi, " duration ", duration_ms, " grayed ", grayed);

                this.pianoroll.range_set(this.tickindex_to_time[ti], this.tickindex_to_time[ti] + duration_ms, midi);
        }

        // vexflow isn't all that nice in how it stores the original input data,
        // you have to collect it back from all over the place instead of simply
        // remembering what the input looked like. This is a bit of a nuisance. 
        //
        // the code below gets passed in all the keys and accidentals for one voice of one 
        // bar x position, then figures out which go with which and passes the result 
        // to the expect_key routine which adds a single key to the list of keys we are 
        // expecting for this position. Because the routine gets called for every voice
        // all voices are merged together. 

        // technically volume should be on a per-key basis, you could play the left
        // hand very quietly while playing the right hand louder or the other way
        // around.
        // TODO

        expect_keys(bar, ti, xpos, voice, tickables, keys, accidentals, duration_ms, volume)

        {
                var acc_by_key = [];

                if (duration_ms > 0) {
                }
                else {
                        debugger;
                }

                for (var key=0;key<keys.length;key++) {
                        acc_by_key[key] = '';
                }

                if (typeof(accidentals) != "undefined") {
                        for (var i=0;i<accidentals.length;i++) {
                                acc_by_key[accidentals[i].index] = accidentals[i].type;
                        }
                }

                for (var key=0;key<keys.length;key++) {
                        this.expect_key(bar, ti, key, xpos, voice, keys[key], acc_by_key[key], duration_ms, volume, tickables.note_heads[key]);
                }
        }

        color_notehead(notehead, color)

        {
                // console.log(notehead);

                notehead.setStyle({fillStyle: color, strokeStyle: color});

                notehead.draw();
        }

        // color all noteheads in a set of expected notes a given color unless they are grayed out

        color_active_noteheads(tickindex, color)

        {

                if (tickindex < 0 || tickindex > this.xposs.length) {
                        return;
                }

                var notes = this.notes[this.xposs[tickindex]];

                // do we still need this with that guard in place above?
                // TODO

                if (typeof(notes) == "undefined") {
                        return;
                }

                for (var i=0;i<notes.length;i++) {
                        if (!notes[i].grayed) {
                                this.color_notehead(notes[i].notehead, color);
                        }
                }
        }

        make_bar(vf, xpos, width) 

        {
                // TODO y is a constant
                var system = vf.System({ x: xpos, y: 60, width: width, spaceBetweenStaves: 10 });
                this.bars.push(system);
                return system;
        }

        // outputs text on the stave, the x coordinate is an absolute one
        // with '0' the beginning of bar 1

        write_text(bar, text,x,y )

        {
                this.bars[bar].context.font="10px Georgia";
                this.bars[bar].context.fillText(text,x - this.bars[bar].canvas.pos ,y);
        }

        // Sort all the notes in a chord in ascending order. This is a workaround
        // for a bug in vexflow where "(C4# A3)/8" is rendered wrong (as opposed to
        // "(A3 C#4)/8" which works correctly).

        sort_chord(s)

        {
                // all possible notes in lexical order

                var allnotes = " A0 An0 A#0 Bb0 B0 Bn0 B#0 " +
                        "Cb1 C1 Cn1 C#1 Db1 D1 Dn1 D#1 Eb1 E1 En1 E#1 Fb1 F1 Fn1 F#1 Gb1 G1 Gn1 G#1 Ab1 A1 An1 A#1 Bb1 B1 Bn1 B#1" +
                        "Cb2 C2 Cn2 C#2 Db2 D2 Dn2 D#2 Eb2 E2 En2 E#2 Fb2 F2 Fn2 F#2 Gb2 G2 Gn2 G#2 Ab2 A2 An2 A#2 Bb2 B2 Bn2 B#2" +
                        "Cb3 C3 Cn3 C#3 Db3 D3 Dn3 D#3 Eb3 E3 En3 E#3 Fb3 F3 Fn3 F#3 Gb3 G3 Gn3 G#3 Ab3 A3 An3 A#3 Bb3 B3 Bn3 B#3" +
                        "Cb4 C4 Cn4 C#4 Db4 D4 Dn4 D#4 Eb4 E4 En4 E#4 Fb4 F4 Fn4 F#4 Gb4 G4 Gn4 G#4 Ab4 A4 An4 A#4 Bb4 B4 Bn4 B#4" +
                        "Cb5 C5 Cn5 C#5 Db5 D5 Dn5 D#5 Eb5 E5 En5 E#5 Fb5 F5 Fn5 F#5 Gb5 G5 Gn5 G#5 Ab5 A5 An5 A#5 Bb5 B5 Bn5 B#5" +
                        "Cb6 C6 Cn6 C#6 Db6 D6 Dn6 D#6 Eb6 E6 En6 E#6 Fb6 F6 Fn6 F#6 Gb6 G6 Gn6 G#6 Ab6 A6 An6 A#6 Bb6 B6 Bn6 B#6" +
                        "Cb7 C7 Cn7 C#7 Db7 D7 Dn7 D#7 Eb7 E7 En7 E#7 Fb7 F7 Fn7 F#7 Gb7 G7 Gn7 G#7 Ab7 A7 An7 A#7 Bb7 B7 Bn7 B#7" +
                        "Cb8 C8 Cn8 C#8 Db8 D8 Dn8 D#8 Eb8 E8 En8 E#8 Fb8 F8 Fn8 F#8 Gb8 G8 Gn8 G#8 Ab8 A8 An8 A#8 Bb8 B8 Bn8 B#8";

                // console.log("sort_chord: input ", s);

                if (s.substr(0,1) != '(') {
                        // not a chord; return as it was

                        // console.log("   not a chord");

                        return s;
                }

                // console.log("   is a chord");

                var p = s.indexOf(")");

                var r = s.substr(p+1);

                var m = s.substr(1,p-1);

                // console.log("   r ", r);
                // console.log("   m ", m);

                var parts = m.split(" ");

                parts.sort((a, b) => allnotes.indexOf(a) - allnotes.indexOf(b));

                // added this to ensure no double notes on make it through

                parts = parts.filter(this.only_unique);

                m = parts.join(" ");

                // console.log("   result: ", "(" + m + ")" + r);

                return "(" + m + ")" + r;
        }

// test cases

//console.assert(sort_chord("(A3 C#4)") == "(A3 C#4)", { errorMsg: 'sort_chord broken'});
//console.assert(sort_chord("(A3 C#4)/16") == "(A3 C#4)/16", { errorMsg: 'sort_chord broken'});
//console.assert(sort_chord("A3") == "A3", { errorMsg: 'sort_chord broken'});
//console.assert(sort_chord("(C#4 A3)") == "(A3 C#4)", { errorMsg: 'sort_chord broken'});
//console.assert(sort_chord("(C#4 A3)/8") == "(A3 C#4)/8", { errorMsg: 'sort_chord broken'});

        sort_easyscore(s)

        {
                var parts = s.split(",");

                for (var i=0;i<parts.length;i++) {
                        parts[i] = this.sort_chord(parts[i]);
                }

                return parts.join(",");
        }

        note_duration(note) 

        {
                if (note.indexOf("/32") > 0) {
                        return 32;
                }

                if (note.indexOf("/16") > 0) {
                        return 16;
                }

                if (note.indexOf("/8") > 0) {
                        return 8;
                }

                if (note.indexOf("/4.") > 0) {
                        return 2.666;
                }

                if (note.indexOf("/4") > 0 || note.indexOf("/q")) {
                        return 4;
                }

                if (note.indexOf("/2.") > 0) {
                        return 1.333;
                }

                if (note.indexOf("/2") > 0) {
                        return 2;
                }

                if (note.indexOf("/1.") > 0) {
                        return 0.666;
                }

                if (note.indexOf("/1") > 0) {
                        return 1;
                }

                console.log(note);

                debugger;

                return "";
        }


        annotate(parts)

        {
                var r = [];

                parts.map(part => 
                        r.push( { duration: this.note_duration(part), note: part})
                );

                // console.log("annotated: ", r);

                return r;
        }

        // determine if we should introduce a break in a beam at
        // this point
        // This should be improved upon. There are pretty
        // strict rules about these groupings and the program
        // should respect them all in so far as they are not
        // conflicting with each other. 
        // For example, in 4/4 time there can be up to 8 32nd notes
        // under a single set of beams. Test cases should be
        // made for all time signatures and all possible groupings.
        // TODO

        grouping_boundary(total, advance)

        {
                // console.log("total " , total , " advance ", advance);

                switch (timesig_num + "/" + timesig_type) {
                        case '6/4':
                                if (total < 0.750 && (total + advance) >= 0.749) {
                                        return true;
                                }
                                break;
                        case '6/8':
                                if (total < 0.375 && (total + advance) >= 0.374) {
                                        return true;
                                }
                                break;
                        case '2/4':
                        case '4/4':
                                if (total < 0.5 && (total + advance) >= 0.49) {
                                        return true;
                                }
                                break;
                        case '3/4':
                                if (total < 0.25 && (total + advance) >= 0.24) {
                                        return true;
                                }

                                if (total < 0.50 && (total + advance) >= 0.49) {
                                        return true;
                                }
                                break;

                        default:
                                return true;

                                break;
                }

                return false;
        }

        is_rest(s)

        {
                return s.note.indexOf("/r") > 0;
        }

        // group transforms an array of notes [a, b, c, d] into
        // a new array where notes that can be beamed are grouped
        // together [a, [b, c], d]

        // This is where the actual beaming happens. Of course, just like anything
        // music theory related it isn't as simple as dumping all the notes into
        // a single beam. Beamed notes have specific groupings depending on the
        // time signature of the piece.
        //
        // https://www.musicnotes.com/now/musictheory/note-beaming-and-grouping-in-music-theory/

        group(parts)

        {
                var debuggroup = false;
                var r = [];
                var boundary = false;

                // the leftmost note of this set of parts is not 
                // necessarily starting immediately at the left
                // hand side of the bar, we need a way to correct
                // for any gaps at the beginning of the bar as well
                // as for gaps between notes. These are called 'rests' ;)

                var total = 0;

                var i = 0;

                if (debuggroup) {
                        console.log("input to group: ", parts);
                }

                while (i<parts.length) {
                        if (debuggroup) {
                                console.log("parts[i].duration: ", parts[i].duration);
                        }

                        if (parts[i].duration < 8 || this.is_rest(parts[i])) {

                                if (debuggroup) {
                                        console.log("parts[i] can't be beamed", parts[i]);
                                }

                                // note can't be beamed
                                r.push(parts[i].note);
                        }
                        else {
                                if (r.length > 0 && Array.isArray(r[r.length-1])) {

                                        // if we are on a border where we should separate 
                                        // this group from the previous one then we
                                        // add an extra empty list to the array
                                        //

                                        // the (r[r.length-1].length == 4 && timesig_num == 4) is a 
                                        // quick hack to stop ridiculously long beams from forming, 
                                        // they look ugly and are probably incorrect anyway so this
                                        // chops them up every fourth note.

                                        if (boundary || (r[r.length-1].length == 4 && timesig_num == 4)) {
                                                r.push([]);
                                        }

                                        // the last element in the array is already an array
                                        // of short notes so we append to it
                                        r[r.length-1].push(parts[i].note);
                                }
                                else {
                                        // empty array or ending on a string so we append a new list
                                        r.push([parts[i].note]);
                                }
                        }

                        boundary = false;

                        if (this.grouping_boundary(total, 1 / parts[i].duration)) {
                                boundary = true;
                        }

                        total = total + (1 / parts[i].duration);
                        i++;
                }

                return r;
        }

        // 8th, 16th and 32nd notes played in succession are
        // joined using beams

        add_beams(easyscore, notes, clef)

        {
                var r = [];

                // shortcut in case we have no notes at all

                if (notes == "") {
                        return [];
                }

                // console.log("notes: ", notes);

                var parts = notes.split(",");

                var parts_annotated = this.annotate(parts);

                var grouped_parts = this.group(parts_annotated);

                // console.log("grouped_parts: ", grouped_parts);

                for (var i=0;i<grouped_parts.length;i++) {
                        if (Array.isArray(grouped_parts[i])) {
                                if (grouped_parts[i].length == 1) {
                                        r = r.concat(easyscore.notes(grouped_parts[i][0], {clef: clef, stem: 'up'}));
                                }
                                else {
                                        if (grouped_parts[i].length == 0) {
                                                debugger;
                                        }

                                        // console.log("trying to beam grouped parts: ", grouped_parts[i].join(","));

                                        r = r.concat(easyscore.beam(easyscore.notes(grouped_parts[i].join(","), {clef: clef, stem: 'up'})));
                                }
                        }
                        else {
                                r = r.concat(easyscore.notes(grouped_parts[i], {clef: clef, stem: 'up'}));
                        }
                }

                // console.log("after adding beams: ", r);

                return r;
        }

        // A3 comes before C4 and so within a chord should be before it or
        // easyscore will balk and will use the wrong notes

        // quick testcase to ensure this comes out ok

        // console.log(sort_easyscore('(A3 C#4)/8,(A3 C#4)/8,(A3 C#4)/8,(C#4 A3)/8,(A3 C#4)/8,(C#4 A3)/8'));

        add_bar(canvas, vf, width, timesig_num, timesig_type, easyscore, left, right)

        {
                // we draw this bar where the previous one ended

                if (this.bars.length) {
                        this.bar_pos[this.bars.length] = this.bar_pos[this.bars.length-1] + this.bar_widths[this.bars.length-1];            // scaled!
                }
                else {
                        this.bar_pos[this.bars.length] = 0;
                }

                this.bar_widths[this.bars.length] = width;     // scaled!
               
                // the bar is offset by the position of this canvas in the
                // row of adjacent canvases
 
                var bar = this.make_bar(vf, this.bar_pos[this.bars.length] - canvas.pos, width);

                // NASTY
                // we inject the canvas information into the bar so that
                // we can recover it later on, all activity within the
                // bar will be relative to the start of the canvas

                bar.canvas = canvas;

                console.log(this.bars.length, "left: ", left, "right: ", right);

                // for instance: 20 "left: " "A3/8[id="16939"],d3/4/r ,A3/8[id="17264"]" "right: " "F#4/8[id="16724"],b4/4/r ,D4/8[id="17049"],b4/4./r ,D4/8[id="17484"]"
                // broken down and sorted by timestamp:
                // note: the rests don't have a timestamp!

                // A3/8[id="16939"]
                //                           F#4/8[id="16724"]
                //                           4/r
                //                           D4/8[id="17049"]
                // 4/r 
                // A3/8[id="17264"] 
                //                           
                //                            4./r
                //                            D4/8[id="17484"]

                // for some reason this renders completely wrong, but at least at this point it still makes sense

                bar.addStave({
                        voices: [
                                easyscore.voice(this.add_beams(easyscore, right, 'treble'))
                        ],
                });

                // console.log("right: ", right);

                bar.addStave({
                        voices: [
                                easyscore.voice(this.add_beams(easyscore, left, 'bass'))
                        ]
                });
        }

        only_unique(value, index, self) {
                return self.indexOf(value) === index;
        }

        // this is pretty arbitrary, there has to be a better
        // way to determine whether or not notes are part of the 
        // same chord

        same_chord(current, note)

        {
                console.log("current: ", current, " note ", note);

                return Math.abs(current-note) < 10;
        }

        quantize_duration(ppq, duration)

        {
                var duration_type = '4';

                // quantization of duration is a bit tricky in that the
                // longer a note is played the more leeway there is with
                // respect to its duration, so a 32nd note has less 'slop'
                // than a quarter note.

                var durations = [
                        {type: "1." , mid: 6*ppq, low: ((6*ppq)+(4*ppq))/2, high: 100*ppq },   // dotted whole note; passe-partout for really long notes
                        {type: "1" , mid: 4*ppq, low: ((4*ppq)+(3*ppq))/2, high: ((4*ppq)+(6*ppq))/2 },   // whole note
                        {type: "2.", mid: 3*ppq, low: ((3*ppq)+(2*ppq))/2, high: ((3*ppq)+(4*ppq))/2 },   // dotted half note
                        {type: "2",  mid: 2*ppq, low: ((2*ppq)+(1.5*ppq))/2, high: ((2*ppq)+(3*ppq))/2 },   // half note
                        {type: "4.", mid: 1.5*ppq, low: ((1.5*ppq)+(ppq))/2, high: ((1.5*ppq)+(2*ppq))/2 },   // dotted quarter note
                        {type: "4", mid: ppq, low: ((ppq)+(.75*ppq))/2, high: ((ppq)+(1.5*ppq))/2 },   // quarter note
                        {type: "8.", mid: .75*ppq, low: ((.75*ppq)+(ppq))/2, high: ((.75*ppq)+(ppq))/2 },   // dotted eigth (is this useful?)
                        {type: "8", mid: .5*ppq, low: ((.5*ppq)+(.25*ppq))/2, high: ((.5*ppq)+(.75*ppq))/2 },   // eighth
                        {type: "16", mid: .25*ppq, low: ((.25*ppq)+(.125*ppq))/2, high: ((.25*ppq)+(.5*ppq))/2 },   // 16th
                        {type: "32", mid: .125*ppq, low: 0, high: ((.125*ppq)+(.25*ppq))/2 }   // 32nd

                ];

                // console.log("quantize_duration: input ppq ", ppq, " duration ", duration);
  
                for (var i=0;i<durations.length;i++) {
                        if (duration > durations[i].low && duration <= durations[i].high) {
                                // found it

                                duration = durations[i].mid;
                                duration_type = durations[i].type;

                                break;
                        }
                }

                // console.log("result: duration ", duration, " duration_type ", duration_type);

                return { duration: duration, duration_type: duration_type};
        }

        // This little routine is the source of many headaches. It is responsible for
        // collecting the notes played in a chord into the same tickable. It does
        // this by swiping all the notes within a short interval of time into the
        // same bucket. If it doesn't work properly you'll see notes that should be
        // in the same chord spread out across multiple tickables, and whenever the
        // score scrolls past such a point it will jerk because it will move *very*
        // rapidly from one note to the next (because according to the midi file 
        // only a very short number of ticks passed between the notes).
        
        // ideally, this routine would have no effect at all but because many midi
        // files are of questionable quality you have to work with some 'slop' in 
        // the input, if we were to use an exact comparison to determine whether
        // notes are part of a chord or not then many midi files would fail to 
        // render properly and for the sound a tiny bit of error usually does not
        // show up.

        rasterize_note_ticks(ppq, ticks)

        {

                return ticks;

                var note_tick = ticks + (ppq / 16);

                note_tick = note_tick - (note_tick % 8); 

/*
                // if the tempo is low then the notes are longer and so
                // the interval should be longer; conversely, if the
                // tempo is very high then the interval should be 
                // shorter

                var interval = ppq / 8;     // start with a sizeable fraction of a quarter note

                // adjust for the tempo

                interval = Math.floor(interval * (5 / this.tick_to_tempo[ticks]));
        
                // if the tempo was 200 interval is now 50
                // if the tempo was 50  interval is now 200

                var note_tick = Math.floor((ticks + (interval / 2)) / interval) * interval;
        
                console.log("       ppq", ppq, "ticks", ticks, "note_tick", note_tick, "interval", interval, "tempo", this.tick_to_tempo[ticks]);
*/
                return note_tick;
        }

        // add up the length of all the individual notes in the
        // bar taking into account the fact that notes that
        // start on the same tick should be counted only once
        // and that gaps between notes where the gap is longer
        // than the duration of the previous note should 
        // include that excess gap.

        total_notes_length(notes)

        {
                if (notes.length == 0) {
                        return 0;
                }

                var l = notes[0].durationTicks;

                for (var j=1;j<notes.length;j++) {
                        if (notes[j].ticks != notes[j-1].ticks) {
                                l += notes[j].durationTicks;
                        }
                }

                return l;
        }

        average_distance(notes)

        {
                var n = 0;
                var d = 0;

                for (var i=1;i<notes.length;i++) {
                        if (notes[i].ticks != notes[i-1].ticks) {
                                d += notes[i].ticks - notes[i-1].ticks;
                                n++;
                        }
                }

                if (n == 0) {
                        n = 1;
                }

                return d / n;
        }

        // this figures out which notes form chords and readjusts
        // their ticks to the average of all the notes in a chord

        group_chords(ppq, ticks_per_bar, notes)

        {
                // if there is only one note then there is no way
                // to form a chord.

                if (notes.length == 1) {
                        return notes;
                }

                var r = [... notes];
               
                var l = this.total_notes_length(notes);

                var d = this.average_distance(notes);

                console.log("length of notes in bar: ", l, "average distance", d, "ticks_per_bar", ticks_per_bar);
 
                for (var i=0;i<notes.length;i++) {


                }

                console.log(notes);

                return r;
        }

// console.log("rasterize_note_ticks(256, 7679)", rasterize_note_ticks(256, 7679));
// console.log("rasterize_note_ticks(256, 7680)", rasterize_note_ticks(256, 7680));

// console.log("rasterize_note_ticks(256, 16129)", rasterize_note_ticks(128, 16129));
// console.log("rasterize_note_ticks(256, 16128)", rasterize_note_ticks(128, 16128));

        note_not_in(r, note)

        {
                for (var i=0;i<r.length;i++) {
                        // same note

                        if (r[i].midi == note.midi) {
                                if (Math.abs(r[i].ticks - note.ticks) < 16) {
                                        return true;
                                }
                        }

                }

                return false;
        }

        remove_duplicate_notes(notes)

        {
                var r = [];

                for (var i=0;i<notes.length;i++) {
                        if (!this.note_not_in(r, notes[i])) {
                                r.push(notes[i]);
                        }
                }

                return r;
        }

        // convert a bar of notes for either hand to an easyscore
        // string. This is not that simple. Notes can start on 
        // arbitrary ticks, can be an arbitrary number of ticks
        // long and even though notes are not started exactly
        // at the same moment they can *still* be part of the 
        // same chord!

        // note that this called twice, once for the left hand
        // and once for the right hand, this makes it a bit
        // tricky to keep both hands in sync with respect to
        // the chords that are split up across multiple hands

        // as a side effect we update the highest tick seen value

        // we need the key signature here!

        bar_to_easyscore(bar, staff, ppq, ticks_per_bar, notes)

        {
                console.log("bar to easyscore: ", bar + 1);

                // disabled for now, this is work in progress that
                // should at some point in the future replace the
                // crummy mechanisms in use right now

                // notes = this.group_chords(ppq, ticks_per_bar, notes);

                // accents needs to start off with the flats/sharps for the 
                // key in use preset otherwise a first note in a bar that
                // is a natural when the score says it isn't would be missed,
                // and would get displayed without a natural sign

                var accents = [... this.keysig_to_bar_initial(keysig)];
                var next_accents = [... this.keysig_to_bar_initial(keysig)];
                var slop = 0;
                var consumed = 0;
                var s = "";

                // first we quantize the starting positions of the notes
                // then we quantize the durations of all notes starting on the same position
                // then we combine them and add rests where necessary

                // console.log(ppq, notes);

                // consume the notes chord by chord
                // assume they are listed in order

                var i = 0;
                var ticknotes = "";
                var n_ticknotes = 0;
                var tick_consumed = 0;
                var current_tick = 0;

                var duration;
                var duration_type;


                // midi files can contain all kinds of weirdness, it is for instance possible to start
                // the same note multiple times on the same tick with different durations. We drop
                // one of the two notes if we detect such a situation.

                notes = this.remove_duplicate_notes(notes);

                // this is the start of the bar

                if (notes.length) {
                        current_tick = this.rasterize_note_ticks(ppq, notes[0].ticks);
                }
                else {
                        current_tick = ticks_per_bar * bar;
                }

                // console.log("before starting bar current_tick is ", current_tick);

                var note_tick;

                while (i<notes.length) {

                        // Align the notes on ppq/4 boundaries. That gives us
                        // sixteenth notes if we need them.

                        note_tick = this.rasterize_note_ticks(ppq, notes[i].ticks);

                        // console.log("note[", i, "].ticks: ", notes[i].ticks, " => ", note_tick);

                        // if the new note starts on a different tick assume we are done
                        // with the previous tick and close it out

                        // console.log("current_tick ", current_tick, " note_tick " , note_tick);

                        if (n_ticknotes > 0 && !this.same_chord(current_tick,note_tick)) {

                                // close out the previous note or chord

                                // console.log("current_tick ", current_tick , " and note_tick ", note_tick, " not in same chord", same_chord(current_tick, note_tick));

                                if (s.length > 0) {
                                        s = s + ",";
                                }

                                // if this is a chord indicate so 

                                if (n_ticknotes > 1) {
                                        s = s + "(" + ticknotes + ")";
                                }
                                else {
                                        s = s + ticknotes;
                                }

                                s = s + "/" + duration_type;

                                // use the ID field to log the original midi tick

                                // why do we use 'current_tick' here, should it not be note_tick?

                                s = s + '[id="' + current_tick + '"]';

                                if (current_tick < (bar * ticks_per_bar)) {
                                        debugger;
                                }

                                consumed = consumed + tick_consumed;

                                // we no longer bother with rests, they just upset the formatter
                                // we won't display them anyway, and the final positioning is
                                // timing dependent

                                // check if we need to insert a rest here because
                                // there is a gap larger than the tick_consumed space between
                                // this note and the next

                                var gap = note_tick - current_tick - tick_consumed;

                                // console.log("gap: ", gap, " current_tick: ", current_tick, " note_tick ", note_tick, " tick consumed ", tick_consumed);

                                if (gap > 32) {
                                        var ndur = this.quantize_duration(ppq, gap);

                                        // console.log(ndur);

                                        if (s.length > 0) {
                                                s = s + ",";
                                        }

                                        // figure out at what height to place the rest

                                        if (staff == treble) {
                                                s = s + "b4/" + ndur.duration_type + "/r ";
                                        }
                                        else {
                                                s = s + "d3/" + ndur.duration_type + "/r ";
                                        }
                                }

                                // prepare for the next note or chord

                                // we copy the accent set used for this tick into the one for the next tick.
                                // that way once a neutral sign is output we do not output it for subsequent
                                // notes of the same pitch.

                                accents = [... next_accents];

                                ticknotes = "";
                                n_ticknotes = 0;
                                tick_consumed = 0;

                                current_tick = note_tick;
                        }
                        else {
                                if (n_ticknotes > 0) {
                                        // console.log("same chord, continuing");
                                }
                        }

                        // if this is a chord then add a space

                        if (n_ticknotes > 0) {
                                ticknotes = ticknotes + " ";
                        }

                        var duration_ticks = notes[i].durationTicks;

                        // fix some errors in the midi input file by
                        // making educated guesses as to what the
                        // real values should have been

                        // extend zero ticks notes so they become audible

                        if (duration_ticks == 0) {
                                // console.log("extended duration of a note from ", duration_ticks, " to ", 64);
                                duration_ticks = 64;
                        }

                        // limit the number of ticks in a note to the remaining number of
                        // ticks in this bar
/*
                        if ((duration_ticks+consumed) > ticks_per_bar) {
                                console.log("limiting a note duration to the bar");

                                duration_ticks = ticks_per_bar - consumed;
                        }
*/
                        // limit a notes' duration to one tick before the next time this
                        // note is played within this bar

                        // this fixes a bug in some midi files that contain NOTE_ON, NOTE_ON, NOTE_OFF
                        // effectively we add an extra NOTE_OFF before the second NOTE_ON

                        for (var j=i+1;j<notes.length;j++) {
                                if (notes[j].midi == notes[i].midi) {
                                        if (notes[i].ticks + duration_ticks > notes[j].ticks) {
                                                // console.log("limiting a note duration to the next time it is played");
       
                                                // console.log("   duration_ticks was ", duration_ticks, " and will be " , (notes[j].ticks - notes[i].ticks) - 1);
 
                                                duration_ticks = (notes[j].ticks - notes[i].ticks)-1;
                                        }
                                }
                        }

                        ndur = this.quantize_duration(ppq, duration_ticks);

                        duration = ndur.duration;
                        duration_type = ndur.duration_type;

                        // ignore extremely short notes
                        // 20 ticks could be quite long if the tempo is long enoug;
                        // convert the duration to milliseconds and judge by that
                        // TODO

                        if (duration < 0) {
                                debugger;
                        }

                        if (duration < 20) {
                                i++;
                                continue;
                        }

                        // add the note to the notes in this tick
                        // this will need more logic to disambiguate between
                        // for instance C# and Db in the key of F Major

                        ticknotes = ticknotes + this.midi_to_key(keysig, accents, next_accents, notes[i].midi);
                        n_ticknotes++;
                        tick_consumed = duration;

                        i++;
                }

                // if there are non-closed out notes left add them now

                // console.log("closing out to s ", s, " n_ticknotes " , n_ticknotes, " ticknotes ", ticknotes);

                if (n_ticknotes) {
        
                        if (s.length > 0) {
                                s = s + ",";
                        }

                        // if this is a chord indicate so 

                        if (n_ticknotes > 1) {
                                // console.log("is a chord: ", ticknotes);

                                s = s + "(" + ticknotes + ")";
                        }
                        else {
                                // console.log("single note: ", ticknotes);

                                s = s + ticknotes;
                        }

                        s = s + "/" + duration_type;

                        // use the id field to log the original midi tick

                        s = s + '[id="' + current_tick + '"]';

                        consumed = consumed + tick_consumed;
                }

                // if the bar is completely empty put at least a rest in it so vexflow won't balk

                if (s.length == 0) {
                        s = "d3/8/r";
                }

                // console.log("consumed: ", consumed);

                s = this.sort_easyscore(s);

                // console.log("result " , s);

                return s;
        }

/*
var test_notes = [
        { name: "A5", octave: 5, pitch: "A", duration: 0.263, time: 33.77, bars: 6.00390625, midi: 81, velocity: 0.6, noteOffVelocity: 0, ticks: 4611, durationTicks: 36 },
        { name: "G5", octave: 5, pitch: "G", duration: 0.219, time: 34.035, bars: 6.05078125, midi: 79, velocity: 0.8, noteOffVelocity: 0, ticks: 4647, durationTicks: 30 },
        { name: "A5", octave: 5, pitch: "A", duration: 4.013, time: 34.255, bars: 6.08984375, midi: 81, velocity: 0.8, noteOffVelocity: 0, ticks: 4677, durationTicks: 548 }];
*/

// console.log("bar to easyscore output: ", bar_to_easyscore(0, treble, 256, 768, test_notes));

        // ticks per bar is strictly speaking variable, the 
        // time signature could change
        // TODO

        ticks_per_bar(ppq, timesig_num, timesig_type)

        {
                var f = (timesig_num / timesig_type) / .25;     // how many quarter notes in a bar

                var tpb = f * ppq;

                // console.log("ticks_per_bar = ", tpb);

                return tpb;
        }

        start_tick_to_bar_number(ppq, timesig_num, timesig_type, tick)

        {
                return Math.floor(tick / this.ticks_per_bar(ppq, timesig_num, timesig_type));
        }

        // this function takes an array of notes and converts it into 
        // easyscore compatible strings

        notes_to_easyscore(staff, ppq, timesig_num, timesig_type, notes)

        {
                var bars = [];

                // console.log(ppq);
                // console.log(timesig_num);
                // console.log(timesig_type);
                // console.log(notes);

                // special case, there are no notes at all in this track so return a single rest. 

                if (notes.length == 0) {
                        return [ "d3/8/r" ];
                }

                // file duration in bars
                // if the time signature changes during the piece this will note work
                // TODO       
 
                var n_bars = Math.floor(notes[notes.length-1].ticks / this.ticks_per_bar(ppq, timesig_num, timesig_type)) + 1;

                // console.log("file duration in bars ", n_bars);

                // create empty bars to hold the notes

                var notes_per_bar = [];

                for (var i=0;i<(n_bars+2);i++) {
                        notes_per_bar[i] = [];
                }

                // copy the notes to the appropriate bars

                for (var i=0;i<notes.length;i++) {
                        notes_per_bar[this.start_tick_to_bar_number(ppq, timesig_num, timesig_type, this.rasterize_note_ticks(ppq, notes[i].ticks))].push(notes[i]);
                }

                // encode the bars

                for (var bar=0;bar<n_bars;bar++) {
                        // console.log("BAR # ", bar+1);

                        bars[bar] = this.bar_to_easyscore(bar, staff, ppq, this.ticks_per_bar(ppq, timesig_num, timesig_type), notes_per_bar[bar]);
                }

                return bars;
        }

        // naive split function C#4 and below go into the left hand for now.

        // TODO this could be much improved, for instance by figuring out what
        // the distance between notes is to ensure that all the notes in a 
        // hand are playable and not spaced too far apart for a normal
        // hand span (octave+1)

        split_hands(notes)

        {
                var left = [];
                var right = [];

                for (var i=0;i<notes.length;i++) {
                        if (notes[i].midi < 60) {
                                left[left.length] = notes[i];
                        }
                        else {
                                right[right.length] = notes[i];
                        }
                }

                return [left, right];
        }

        count_below_60(notes)

        {
                var n = 0;

                // console.log(notes);

                for (var i=0;i<notes.length;i++) {
                        if (notes[i].midi < 60) {
                                n++;
                        }
                }

                return n;
        }

	track_has_notes(track)

	{
		if (track.notes.length > 0) {
			return true;
		}

		return false;
	}

        extract_notes_from_tracks(tracks)

        {
		console.log("extract notes from tracks");

                var n_tracks_with_notes = 0;

                var tracks_with_notes = [];

                var left_notes;
                var right_notes;

		console.log("tracks.length " , tracks.length);

                switch (tracks.length) {
                        case 0:
				console.log("no notes");
                                return [[], []];

                        case 1:
				console.log("single track, splitting hands");
                                // a single track

                                return this.split_hands(tracks[0].notes);

                        case 3:
				console.log("three tracks with notes?");

				console.log(tracks[2]);

                                if (this.track_has_notes(tracks[2])) {
                                        tracks_with_notes[n_tracks_with_notes++] = 2;
                                }
                                // falls through
                        case 2:

				console.log("two tracks with notes?");

				console.log(tracks[1]);

                                if (this.track_has_notes(tracks[1])) {
                                        tracks_with_notes[n_tracks_with_notes++] = 1;
                                }
                                // falls through
                        default:

				console.log("one track with notes?");

                                console.log(tracks[0]);

                                if (this.track_has_notes(tracks[0])) {
                                        tracks_with_notes[n_tracks_with_notes++] = 0;
                                }

                                break;
                }

		console.log("tracks_with_notes: ", tracks_with_notes);

                switch (n_tracks_with_notes) {
                        case 1:
                                return this.split_hands(tracks[tracks_with_notes[0]].notes);
                        case 2:

                                // auto detect which track belongs to which hand
                                // TODO slight chance that *both* hands are below 60 all the time;
                                // also it might be that there are a lot of low notes in the right
                                // hand. Is there a better way of determining this?

                                if (this.count_below_60(tracks[1].notes) > this.count_below_60(tracks[0].notes)) {
                                        left_notes = tracks[1].notes;
                                        right_notes = tracks[0].notes;
                                }
                                else {
                                        left_notes = tracks[0].notes;
                                        right_notes = tracks[1].notes;
                                }

                                return [left_notes, right_notes];
                }

                return [[], []];
        }
        
        tickable_is_rest(tickable)
        
        {
                if (tickable.noteType == "r") {
                        return true;
                }
        
                if (Math.floor(tickable.attrs.id) > 0) {
                    return false;
                }
                
                if (isNaN(Math.floor(tickable.attrs.id))) {
                    return true;
                }
                
                return false;
        }

        midi_to_easyscore(ppq, timesig_num, timesig_type, tracks)

        {
                var left_notes;
                var right_notes;

                // two tracks, assume one is left hand and one is right hand
                // would be good to figure out which is which!

                // console.log(tracks.length);

                [left_notes, right_notes] = this.extract_notes_from_tracks(tracks);

                // console.log("left_notes: ", left_notes, " right_notes: " , right_notes);

                // copy the notes to their respective bars and encode them as easyscore strings

                var left = this.notes_to_easyscore(bass, ppq, timesig_num, timesig_type, left_notes);

                // console.log("left easyscore strings ", left);

                console.log("right notes: ", right_notes);

                var right = this.notes_to_easyscore(treble, ppq, timesig_num, timesig_type, right_notes);

                // console.log("right easyscore strings ", right);

                // console.log("left.length " , left.length, " right.length ", right.length);

                var r = [];

                // merge
                // Deal with all three combinations of LR R and L
                // in case one hand ends earlier than the other.

                var l = Math.max(left.length, right.length);

                // console.log("merging");

                for (var i=0;i<l;i++) {
                        if (i < left.length && i < right.length) {
                                // console.log("both");
                                r[i] = [left[i], right[i]];
                        }
                        else {
                                if (i < left.length) {
                                        r[i] = [left[i], ""];
                                        // console.log("only left");
                                }
                                else {
                                        if (i < right.length) {
                                                r[i] = ["", right[i]];
                                                // console.log("only right");
                                        }
                                }
                        }
                }

                // console.log("r.length: ", r.length);
                // console.log(r);

                return r;
        }

        // figure out if a tickable contains sharps or flats

        sharp_or_flat(s)

        {
                if (s.indexOf("#") > 0) {
                        return true;
                }

                if (s.indexOf("b") > 0) {
                        return true;
                }

                return false;
        }

        // count how many tickables there are in one easyscore string
        // this is easy because the tickables are separated by commas
        // also add a factor for each tickable that has a sharp or a flat

        bar_complexity(s)

        {
                var complexity;

                var parts = s.split(",");

                complexity = parts.length;

                for (var i=0;i<parts.length;i++) {
                        // TODO
                        // this needs to take into account explicit neutrals too!
                        if (this.sharp_or_flat(parts[i])) {
                                complexity += .5;
                        }
                }

                return complexity;
        }

        // TODO 
        // Need to deal with dotted notes here.
        // this should return a length in milliseconds

        duration_to_ms(tempo, duration)

        {
                var t = 0.25;

                switch (duration) {
                        case '1':
                        case 'w':
                        case 1:
                                t = 4.0;
                                break;
                        case '1.':
                        case 'w.':
                                t = 6.0;
                                break;
                        case '2':
                        case 'h':
                        case 2:
                                t = 2.0;
                                break;
                        case '2.':
                        case 'h.':
                                t = 3.0;
                                break;
                        case '4':
                        case 'q':
                        case 4:
                                t = 1.0;
                                break;
                        case '4.':
                        case 'q.':
                                t = 1.5;
                                break;
                        case '8':
                        case 'e':
                        case 8:
                                t = 0.5;
                                break;
                        case '8.':
                        case 'e.':
                                t = 0.75;
                                break;
                        case '16':
                        case 's':
                        case 16:
                                t = 0.25;
                                break;
                        case '32':
                        case 't':
                        case 32:
                                t = 0.125;
                                break;
                }

                // we now have the fraction of a note in t as a float

                var ms_per_beat = 60000 / tempo;

                // adjust for time signature

                return ms_per_beat * t;
        }

// console.log("whole note: ", duration_to_ms(120, 1));
// console.log("half note: ", duration_to_ms(120, 'h'));
// console.log("quarter note: ", duration_to_ms(120, 'q'));

        ticks_to_ms(tempo, ticks)

        {
                var ms_per_quarter = this.duration_to_ms(tempo, 'q');
                var ms_per_tick = ms_per_quarter / this.pulses_per_quarter;
                var tick_ms = ticks * ms_per_tick;

                return tick_ms;
        }

// some test cases

// console.log("ticks_per_bar 100 4 4 ", ticks_per_bar(100, 4, 4));
// console.log("ticks_per_bar 100 6 4 ", ticks_per_bar(100, 6, 4));
// console.log("ticks_per_bar 100 3 2 ", ticks_per_bar(100, 3, 2));
// console.log("ticks_per_bar 100 3 4 ", ticks_per_bar(100, 3, 4));

        // draw the lines between the bars

        draw_bar_lines()

        {
                for (var bar=0;bar<this.bars.length-1;bar++) {

                        // draw the vertical lines in the staves

                        // normal bar separator

                        var lineRight = new VF.StaveConnector(this.bars[bar].parts[0].stave, this.bars[bar].parts[1].stave).setType(0);
                        lineRight.setContext(this.bars[bar].context).draw();
                }

                var lastbar = this.bars[this.bars.length-1];

                console.log("lastbar context:", lastbar.context);

                // connect the last bar staves vertically
                // with an 'end piece' marker

                var lineRight = new VF.StaveConnector(lastbar.parts[0].stave, lastbar.parts[1].stave).setType(6);
                var lineLeft = new VF.StaveConnector(lastbar.parts[0].stave, lastbar.parts[1].stave).setType(7);

                lineRight.setContext(lastbar.context).draw();
                lineLeft.setContext(lastbar.context).draw();
        }

        // retrieve_notes will take all of the notes that have been 
        // drawn in the bars and will make a list-per-x-position in the
        // notes array. Notes that can't or won't be played will 
        // be grayed out. This can't be done earlier because there it 
        // is not yet known which tickindices will be matched with 
        // which notes. 

        retrieve_notes()

        {
                // notes holds an array of lists each of which shadows one 'xposs' 
                // with all the notes the user should play at that position (and preferably no
                // others unless they are notes that would normally be held at that position!)
                //
                // This is one of the reasons why pianobooster has such a hard time recovering
                // sometimes when a long note is held, as long as you don't release that note
                // it thinks you are playing something wrong even though if you played the 
                // piece from the beginning you actually would be holding that note down at
                // that point in time. 

                this.notes = [];

                // set up a blank array of expected keys for every possible x position

                for (var i=0;i<this.xposs.length;i++) {
                        this.notes[this.xposs[i]] = [];
                }

                for (var bar=0;bar<this.bars.length;bar++) {
			this.bars[bar].tickindices = [];

                        for (var voice=0;voice<this.bars[bar].formatter.voices.length;voice++) {
                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
                                        // ignore rests

                                        // rests can't be played.

                                        if (!this.tickable_is_rest(this.bars[bar].formatter.voices[voice].tickables[tickable])) {
                                                // add all the keys that belong to this x position in this voice to
                                                // the 'notes' arrays
                                                //
                                                // this should take the hand into account! (we may have a keyboard that
                                                // can send events for multiple voices such as an organ keyboard)

                                                // highly annoying we are modifying the 'grayed out' flag in expect_keys because
                                                // it can see which keys can be played and which can't. 

                                                // so we end up overdrawing the noteheads drawn earlier

                                                // first retrieve the x position of the first notehead so we know when 
                                                // the scrolling display of notes hits the hairline for this note

                                                var xp = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX());

                                                // weird, in the muppets theme I need this to ensure that 
                                                // we don't crash; this bug was introduced after adding rests to 
                                                // the bookkeeping
                                                // TODO

                                                if (isNaN(xp)) {
                                                        continue;
                                                }

                                                // offset with the canvas position in the row of canvases
                                                // gives an absolute x position.

                                                xp += this.bars[bar].canvas.pos;

                                                var duration = this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].duration;

                                                // compute the duration of this note based on the current tempo

                                                var midi_tick = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id);

                                                // check if the note is a dotted note; if so extend it with a '.'
                                                if (this.bars[bar].formatter.voices[voice].tickables[tickable].modifierContext.getModifiers('dots')) {
                                                        duration = duration + ".";
                                                }

                                                // console.log("duration: ", duration, "midi_tick", midi_tick, "this.tick_to_tempo.length", this.tick_to_tempo.length, this.tick_to_tempo[midi_tick]);

                                                var duration_ms = this.duration_to_ms(this.tick_to_tempo[midi_tick], duration);

                                                var accidentals = this.bars[bar].formatter.voices[voice].tickables[tickable].modifierContext.getModifiers('accidentals');

                                                // then add all of the noteheads here to the list of expected keys (assuming they are all playable)

                                                this.expect_keys(bar, this.xposs_to_tickindex[xp], xp, voice, this.bars[bar].formatter.voices[voice].tickables[tickable], this.bars[bar].formatter.voices[voice].tickables[tickable].keyProps,accidentals, duration_ms);
						this.bars[bar].tickindices.push(this.xposs_to_tickindex[xp]);
					}
                                }
                        }

			this.bars[bar].tickindices = this.bars[bar].tickindices.sort((a, b) => a - b);

                        this.bars[bar].tickindices = this.bars[bar].tickindices.filter( this.only_unique );
                }

                // after all the voices and bars are done sort the notes in the notes 
                // array by midi value so we can match up chords easier

                // this should be done using a map
                // TODO

                for (var i=0;i<this.scorewidthpx;i++) {
                        if (this.notes.includes(i)) {
                                this.notes[i] = this.notes[i].sort((a, b) => a.midi - b.midi);
                        }
                }

                // console.log("after sort");

                // console.log(notes);

                // console.log(pianoroll);
        }

        // each note that is in the score has been placed, but not all of
        // them will be in the right order, nor will they be neccesarily
        // playable. This function moves the noteheads relative to each
        // other so the order is correct and noteheads are spaced a
        // distance apart that is proportional to the amount of time
        // between when they are played.

        draw_notes_on_time()

        {
                // the number of ticks per bar is for now a constant during the 
                // piece, eventually this should become a per-tick item to ensure
                // that tempo changes during the piece are properly followed.

                var bar_ticks = this.ticks_per_bar(this.pulses_per_quarter, timesig_num, timesig_type);

                // now find all the notes, determine where they should have been drawn and draw them again
                // this is pretty kludgy, especially since we will do it once more! This iteration draws
                // the notes based on their timestamps, the next iteration will adjust them based on 
                // their width.

                for (var bar=0;bar<this.bars.length;bar++) {

                        // reference for the bar itself to determine where to place the notes within the bar

                        var bar_start_ticks = bar * bar_ticks;

                        // draw all according to the timestamps
			//
                        for (var voice=0;voice<this.bars[bar].formatter.voices.length;voice++) {

                                // changed this, it looks wrong outside of the per-voice loop

                                var prevtick = 0;

                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
                                        // ignore rests

                                        // we probably should draw them, but for now just ignore them.
                                    
                                        // tricky, the noteType "r" test does not seem to be bullet proof, sometimes rests appear with a note type 'n' instead of 'r', added
                                        // a check on the ID field to make sure that it is numeric
                                    
                                        if (!this.tickable_is_rest(this.bars[bar].formatter.voices[voice].tickables[tickable])) {

                                                // now that we've used vexflow to draw all the notes we will fix their 
                                                // positions because vexflow is quite hopeless at that, which is strange
                                                // given that it excels at the actual engraving portion of the problem,
                                                // which to me seems to be a much harder one.

                                                // every note has their position recomputed based on the orinal midi ticks
                                                // this will restore all notes in their proper relative order

                                                // recover the original midi tick number; stored in the note ID

                                                var org_tick = this.rasterize_note_ticks(this.pulses_per_quarter, Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id));

                                                console.log("bar: " , bar , " bar_start_ticks ", bar_start_ticks , " org_tick " , org_tick, org_tick-prevtick, this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id);

						prevtick = org_tick;

                                                if (org_tick < bar_start_ticks) {
                                                        // this should never ever happen.
                                                        debugger;
                                                }

                                                // this 'setX' uses the position relative to the beginning of the bar

                                                // the -15 slightly reduces the usable width of the bar which allows us to place
                                                // a note right at the end and still show it within the proper bar.

                                                var pixels_per_tick = (this.bar_widths[bar]-15) / bar_ticks;

                                                var x = (org_tick - bar_start_ticks) * pixels_per_tick;

                                                // stay within the allocated space for this bar

                                                if (x > this.bar_widths[bar]) {
                                                        x = this.bar_widths[bar]-1;
                                                }

                                                this.bars[bar].formatter.voices[voice].tickables[tickable].getTickContext().setX(x);

                                                // draw the note or chord in its time based position

                                                this.bars[bar].formatter.voices[voice].tickables[tickable].draw();
                                        }
                                }
                        }
                }

        }

        // figure out the highest tick number we will need to set up the lookup tables

        highest_tick_in_use(midi)

        {
                var highest_tick = 0;

                if (midi) {

                        for (var i=0;i<midi.tracks.length;i++) {
                                var notes = midi.tracks[i].notes;

                                for (var n=0;n<notes.length;n++) {
				        if (typeof(notes[n]) != "undefined") {
                                                if (notes[n].ticks + notes[n].durationTicks > highest_tick) {
                                                        highest_tick = notes[n].ticks + notes[n].durationTicks;
                                                }
                                        }       
				}
                        }

                        if (highest_tick == 0) {
                                console.log(midi);
                                debugger;
                        }
                }

                if (highest_tick < 500000) {
                        console.log("highest_tick_in_use: faking it...");
                        // fake it! hopefully that's enough...
                        highest_tick = 500000;
                }

                return highest_tick;
        }

        setup_tick_translation_tables(midi)

        {
                // make a quick lookup table for midi tick number to bpm

                this.tick_to_tempo = [];

                // tick to time since start of piece lookup table

                this.tick_to_time = [];

                var j = 0;

                var tempo = 120;

                if (midi) {
                        if (midi.header.tempos.length > 0) {
                                tempo = midi.header.tempos[j].bpm;
                        }
                }

                var clocktime = 0;

		// this is the highest *midi* tick number in use

                var highest_tick = this.highest_tick_in_use(midi);

                // that 4000 can probably go now
                // TODO

                for (var i=0;i<highest_tick + 4000;i++) {
                        if (midi && j < midi.header.tempos.length) {
                                if (i == midi.header.tempos[j].ticks) {
                                        tempo = Math.floor(midi.header.tempos[j].bpm + 0.0001);
                                        j++;
                                }
                        }
                        this.tick_to_tempo[i] = tempo;
                        this.tick_to_time[i] = clocktime;

                        // track the clock through ticks and tempo changes

                        clocktime += this.ticks_to_ms(tempo, 1);
                }

                console.log("at the end of setup, clocktime is ", clocktime);

                return clocktime;
        }
        
        // this function is a debug aid, it shows the positions of all notes in all bars
        
        show_positions()

        {
                for (var bar=0;bar<this.bars.length;bar++) {
                    
                        console.log("show_positions bar ", bar + 1);

                        // collect all x positions where noteheads live within this bar

                        for (var voice=0;voice<this.bars[bar].formatter.voices.length;voice++) {
                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
					if (this.bars[bar].formatter.voices[voice].tickables[tickable].noteType != "r") {
					// console.log(this.bars[bar].formatter.voices[voice].tickables[tickable]);
                                            var x = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX());
                                            console.log("    x is ", x, "origin", this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id, "keyprops", this.bars[bar].formatter.voices[voice].tickables[tickable].keyProps, "notetype", this.bars[bar].formatter.voices[voice].tickables[tickable].noteType);
					}
				}
                        }
                }
        }
        
        // reformat the notes to use the space optimally. This is to avoid awkward blank spaces and crowds
        // elsewhere on the staves, to get to a more balanced use of the white space available on the staves.
        
        // instead of    note,note,note...........................
        // it should be  .....note.........note..........note.....
        // after this routine has run.
        
        // Note order should be invariant, so, should never ever
        // change as a result of this operation, if that happens
        // it is a bug.

        reformat_notes(midi, easyscore_notes, clocktime)

        {
                console.log("reformat_notes, clocktime is ", clocktime);
                this.pianoroll = new Pianoroll(Math.floor(clocktime / 1000) + 1);

                // next pass, now that all notes are in the proper order 
                // we can figure out how to use the space the best

                this.xposs_to_time = [];

                this.xposs = [];

                var cur_tempo = 0;

                for (var bar=0;bar<this.bars.length;bar++) {

			// console.log("reformatting " , bar);

			// console.log("       ", easyscore_notes[bar]);

                        // compute the new formatting map for this bar
                        // maybe we can bring this out of the loop
                        // it's a bit ugly space wise
                        // TODO

                        // this is not a simple problem. The voices do not necessarily contain the
                        // same number of tickables, but you *do* want them to end up in the same
                        // x position if the notes in the voices are to be played at the same moment.

                        //
                        // for instance:
                        //
                        // voice 1                 C            D             E
                        // voice 2                 B                          G
                        //
                        // can be reformatted any way you want as long as the C and the B stick together the order of 
                        // CDE isn't changed and E and G stick together.

                        var org_x = [];
                        var voice_x = [];

                        // collect all x positions where noteheads live within this bar

                        for (var voice=0;voice<this.bars[bar].formatter.voices.length;voice++) {
                                voice_x[voice] = [];
                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
					if (this.bars[bar].formatter.voices[voice].tickables[tickable].noteType != "r") {
					// console.log(this.bars[bar].formatter.voices[voice].tickables[tickable]);
					voice_x[voice][tickable] = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX());
                                        org_x.push(Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX()));

					}
				}
                        }

                        // sort and unique them

                        org_x = org_x.sort((a, b) => a - b);

                        org_x = org_x.filter( this.only_unique );

                        if (org_x.length == 0) {
                                continue;
                        }

                        // we should figure out here how many of the entries we will drop because they
                        // are too close; this will look nicer because it will avoid blank space at the end
                        // of a bar
                        // TODO

                        // we now have an array in org_x with one entry for each occupied 'x' position of all
                        // the tickables.

                        // now we map these onto new coordinates.

                        var adj_x = [];

                        // what is that -15 all about?
                        // TODO

                        var pixels_per_tickable = ((this.bar_widths[bar]) / (org_x.length));

                        // there has to be a nicer way of doing this
                        // TODO

                        var nt = 0;

                        for (var i=0;i<org_x.length;i++) {
                                adj_x[org_x[i]] = Math.floor(pixels_per_tickable * nt)-10;

                                if (i > 0) {
                                        // very closely spaced notes are played together
                                        if ((org_x[i] - org_x[i-1]) > 2) {
                                                nt++;
                                        }
                                        else {
                                                // especially the double write; re. nicer way
                                                adj_x[org_x[i]] = Math.floor(pixels_per_tickable * (nt-1))-10;
                                        }
                                }
                                else {
                                        nt++;
                                }
                        }

                        // console.log("voice_x ", voice_x);
                        // console.log("org_x ", org_x);
                        // console.log("adj_x: ", adj_x);

                        // and then we redraw all the tickables in this voice

                        for (var voice=0;voice<this.bars[bar].formatter.voices.length;voice++) {

                                var cur_tick = 0;
                                var clocktime = 0;

                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
                                        // ignore rests

                                        // we probably should draw them, but for now just ignore them.

                                        if (!this.tickable_is_rest(this.bars[bar].formatter.voices[voice].tickables[tickable])) {

                                                // recover the original midi tick number; stored in the note ID

                                                var org_tick = this.rasterize_note_ticks(this.pulses_per_quarter, Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id));						

                                                // adjust the x position of the tickable

                                                var x = adj_x[voice_x[voice][tickable]];

						console.log("org_tick: " , org_tick, "x", x);

                                                this.bars[bar].formatter.voices[voice].tickables[tickable].getTickContext().setX(x);

                                                // draw the note or chord in its final position

                                                // if a note ends up unplayable we will overwrite the notehead(s) in gray

                                                this.bars[bar].formatter.voices[voice].tickables[tickable].draw();

                                                // retrieve the x position of the next notehead, rounded down to an int because we
                                                // can't do fractions of a pixel anyway and that makes exact matches possible.

                                                var xp = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX());

                                                xp += this.bars[bar].canvas.pos;

                                                // the tick position in milliseconds relative to the beginning of the piece
                                                // that's needed for accurate playback

                                                this.xposs_to_time[xp] = this.tick_to_time[org_tick];

                                                // build up a map of all the x positions where tickables reside

                                                this.xposs.push(xp);

                                                // draw tempo indications

                                                if (voice == 0) {
                                                        if ((bar == 0 && tickable == 0) || cur_tempo != this.tick_to_tempo[org_tick] ) {
                                                                // tempo change!

                                                                // console.log("org_tick: ", org_tick);

                                                                cur_tempo = this.tick_to_tempo[org_tick];

                                                                this.write_text(bar, cur_tempo + " bpm", xp, 30);
                                                        }
                                                }
                                        }
                                }
                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {

                                        if (this.bars[bar].formatter.voices[voice].tickables[tickable].noteType != "r") {
                                                // first we move all the notes *then* we draw the beams
                                                // otherwise a beam will run from a moved note on the left
                                                // to all the notes that were not yet moved

                                                if (this.bars[bar].formatter.voices[voice].tickables[tickable].beam) {
                                                        this.bars[bar].formatter.voices[voice].tickables[tickable].beam.postFormatted = false;

                                                        this.bars[bar].formatter.voices[voice].tickables[tickable].beam.draw();
                                                }
                                        }
                                }

				// console.log("");
                        }

                        // console.log(this.bars[bar]);
                }

                // sort and unique xposs, this leaves one entry for each tickindex
                // with the x position of the noteheads as the value

                this.xposs = this.xposs.sort((a, b) => a - b);
                
                // console.log("xposs after sort:", this.xposs);
                
                this.xposs = this.xposs.filter( this.only_unique );
                
                // console.log("xposs after unique:", this. xposs);
        }

        erase_canvas_draw_staves()

        {
                // erase the canvas (again...); isn't there a nicer way of doing this?

                $('#vexcontainer canvas').each(function(idx, item) {

                        // console.log("clearing ", item.id);

                        var context = item.getContext("2d");

                        context.clearRect(0, 0, item.width, item.height);
                        context.beginPath();
                });

                // first draw all the staves, then draw the notes on top

                // console.log(this.bars);

                for (var bar=0;bar<this.bars.length;bar++) {

                        // console.log("bar # ", bar);

                        // show bar number above the top stave barline

                        this.write_text(bar, bar+1, this.bar_pos[bar], 95);

                        // console.log("formatter ", this.bars[bar].formatter);

                        this.bars[bar].formatter.voices[0].stave.draw();
                        this.bars[bar].formatter.voices[1].stave.draw();
                }
        }

        //
        // now that we have a visual representation of the music extract out
        // the visual and the note information and store it in a way that is
        // easy to interact with for the rest of the program; this so we
        // don't need to work with bars or other musical constructs but just
        // with coordinates; times and note values
        //
        // This should be called every time the score changes or if there
        // is a change to what can/will be played such as when changing
        // the keyboard number of keys available or when the hand to 
        // practice changes.
        //
        // Based on that information certain notes may end up being 
        // grayed out or made available again.
        //

        reposition_notes(midi, easyscore_notes, clocktime)

        {
                console.log("before draw_notes_on_time");
                
                this.show_positions();
            
                // draw notes in order of their timing

                this.erase_canvas_draw_staves();

                this.draw_notes_on_time();

                console.log("before reformat_notes");
                
                this.show_positions();
                
                // then move them to use the available space as good as possible

                this.erase_canvas_draw_staves();
                
                this.reformat_notes(midi, easyscore_notes, clocktime);

                console.log("after reformat notes");
                
                this.show_positions();
                
                // ensure last_tickindex is clamped to the end of the piece
                // this sneakily ensures that if the slider is active that
                // the last_tickindex will not be changed.

                if (last_tickindex > this.xposs.length-1) {
                        // if we are changing the last_tickindex should we redraw the sliders?
                        // TODO
                        last_tickindex = this.xposs.length - 1;
                }

                // create some convenient lookup tables used during the practicing
                // and playback of the piece

                this.xposs_to_tickindex = [];
                this.tickindex_to_time = [];

                for (var i=0;i<this.xposs.length;i++) {
                        this.tickindex_to_time[i] = this.xposs_to_time[this.xposs[i]];
                        this.xposs_to_tickindex[this.xposs[i]] = i;
                }

                // console.log(tickindex_to_time);

                this.draw_bar_lines();
        }

        // public function to do a lookup for a particular
        // x position on the canvas(es); to convert it to a
        // tickindex. If the canvas position does not correspond
        // with a tickable in the score then this will return undefined

        xpos_to_tickindex(x)

        {
                return this.xposs_to_tickindex[x];
        }

        allowed_to_sound(tickindex, sounding) 

        {
                if (this.pianoroll.is_set(this.tickindex_to_time[tickindex], sounding)) {
                        return true;
                }
                return false;
        }

        // compute the interval between two tick indices in milliseconds

        interval(c, p)

        {
                return this.tickindex_to_time[c] - this.tickindex_to_time[p];
        }

        notes_at_x(x)

        {
                return this.notes[x];
        }

        notes_at_tickindex(tickindex)

        {
                return this.notes_at_x(this.xposs[tickindex]);
        }

	// return the number of notes at a tickindex that is not grayed out

	n_active_notes_at_tickindex(tickindex)

	{
        	var notes = this.notes_at_tickindex(tickindex);
		var n = 0;

        	for (var i=0;i<notes.length;i++) {
                	if (!notes[i].grayed) {
				n++;
                	}
        	}

		return n;
	}

	n_notes_for_hand_at_tickindex(hand, tickindex) 

	{
		var notes = this.notes_at_tickindex(tickindex);
		var n = 0;

		for (var i=0;i<notes.length;i++) {
			if (hand == bothhands) {
				n++;
			}
			else {
				if (notes[i].hand == hand) {
					n++;
				}
			}
		}

		return n;
	}

        tickindex_to_x(tickindex)

        {
                return this.xposs[tickindex];
        }

        // this converts the score structure into a long
        // json string that can be used to compare with
        // during unit tests

        dump()

        {
                var r = [];

                for (var bar=0;bar<this.bars.length;bar++) {
                       
                        var allnotes = [];
 
                        for (var voice=this.bars[bar].formatter.voices.length-1;voice >= 0;voice--) {
                                for (var tickable=0;tickable<this.bars[bar].formatter.voices[voice].tickables.length;tickable++) {
                                        if (!this.tickable_is_rest(this.bars[bar].formatter.voices[voice].tickables[tickable])) {
                                            var x = Math.floor(this.bars[bar].formatter.voices[voice].tickables[tickable].note_heads[0].getAbsoluteX());
                                            var notes = '';

                                            for (var n=0;n<this.bars[bar].formatter.voices[voice].tickables[tickable].keyProps.length;n++) {
                                                var p = this.bars[bar].formatter.voices[voice].tickables[tickable].keyProps[n];
                                                if (notes != '') {
                                                        notes = notes + ",";
                                                }
                                                notes = notes + p.int_value;
                                            }

                                            if (allnotes[x] == null) {
                                                allnotes[x] = "X " + x + " T " + this.bars[bar].formatter.voices[voice].tickables[tickable].attrs.id;
                                            }

                                            if (voice == 0) {
                                                allnotes[x] = allnotes[x] + " RH ";
                                            }
                                            else {
                                                allnotes[x] = allnotes[x] + " LH ";
                                            }

                                            allnotes[x] = allnotes[x] + notes;
                                        }
                                }
                        }

                        var s = "";

                        for (var i=0;i<allnotes.length;i++) {
                                if (allnotes[i]) {
                                        if (s != "") {
                                                s = s + ",";
                                        }
                                        s = s + "(" + allnotes[i] + ")";
                                }
                        }

                        r.push({ bar: (bar + 1), notes: s });
                }

                var json = JSON.stringify(r);

                console.log("scoredump", json);

                return json;
        }

        // vexflow renderer of the score

        // the score is passed in as a json encoded midi object in the global variable 'score'
        // an array of canvases to draw the bars on and a maximum width for every canvas that
        // it uses. Canvases may be smaller but never larger than that.

        // the difficulty factor pre-determines how many notes a user
        // will be required to play. If more notes than that are
        // required then the remainder will be grayed out.

        // One major problem right now is that the output is graphical, and that there is
        // no way to inspect the intermediary stages to see where a problem occurs.

        // returns a json string with the output of the formatter, this can be used
        // to compare during unit tests with known input to see if the formatter is
        // working as it should and to prevent flip-flop bugs and regressions.

        constructor(midi, canvases, maxwidth, diff)

        {
                // defaults in case the score does not supply them

                console.log("score.js, midi: ", midi);

                keysig = "C";
                scale = "major";

                timesig_num = 4;
                timesig_type = 4;

                this.diff = diff;

                VF = Vex.Flow;

                var easyscore_notes = [];

                this.bars = [];

                this.bar_widths = [];
                this.bar_pos = [];

                this.canvases = canvases;

                var clocktime;

                if (midi) {
                        this.pulses_per_quarter = midi.header.ppq;

                        // console.log(midi.header.keySignatures);

                        if (midi.header.keySignatures.length > 0) {
                                keysig = midi.header.keySignatures[0].key;
                                scale  = midi.header.keySignatures[0].scale;
                        }

                        if (midi.header.timeSignatures.length) {
                                timesig_num  = midi.header.timeSignatures[0].timeSignature[0];
                                timesig_type = midi.header.timeSignatures[0].timeSignature[1]
                        }

                        clocktime = this.setup_tick_translation_tables(midi);

                        easyscore_notes = this.midi_to_easyscore(midi.header.ppq, timesig_num, timesig_type, midi.tracks);
                }
                else {
                        timesig_num = 4;
                        timesig_type = 4;

                        // some fake data to fill the screen
                        // TODO
                        // put a json version of a nice piece of music in, for instance
                        // a public domain childrens song. 
                        easyscore_notes = [
                                        ['C4/q[id="128"]','']
                        ];

                        this.pulses_per_quarter = 480;

                        clocktime = this.setup_tick_translation_tables(midi);
                }

                timesig = timesig_num + "/" + timesig_type;

                // A major limitation of the CANVAS element is that it has a browser dependent
                // maximum width. Since we're targeting Chrome we can use about 32K pixels,
                // and because we have scaled up by a factor of two it is effectively 16K.

                // So every so many pixels we need to use another CANVAS element to avoid
                // running into that limitation. 

                var canvas = 0;
                
                canvases[canvas].width = 0;
                canvases[canvas].pos = 0;
                $("#" + element_id).width = maxwidth + "px";

                this.scorewidthpx = 0;

                for (var i=0;i<easyscore_notes.length;i++) {

                        // figure out how complex the bar is

                        var complexity = Math.max(this.bar_complexity(easyscore_notes[i][0]), this.bar_complexity(easyscore_notes[i][1]));

                        // 25 pixels per unit of complexity; this should not be a hardcoded constant
                        // TODO

                        var bar_width = 25 * complexity;

                        // lower limit on the size of a bar; this should not be a hardcoded constant
                        // TODO

                        if (bar_width < 200) {
                                bar_width = 200;
                        }

                        if ((canvases[canvas].width + (bar_width)) > maxwidth) {

                                canvas++;

                                // make sure we do not overrun the number of available canvases

                                if (canvas >= canvases.length) {
                                        break;
                                }

                                // starting position of the new canvas


                                canvases[canvas].pos = canvases[canvas-1].pos + Math.floor(canvases[canvas-1].width);
                                canvases[canvas].width = 0;

                                $("#" + element_id).left = 0;
                                $("#" + element_id).width = maxwidth + "px";
                        }

                        var element_id = canvases[canvas].canvas;

                        // console.log("canvas ", canvas , " element_id ", element_id);
                        // console.log(canvases[canvas].canvas);

                        // console.log("n_bars: ", this.bars.length);

                        this.scorewidthpx += bar_width;   // remember the scale factor BUG??

                        canvases[canvas].width += bar_width;

                        var vf = new VF.Factory({renderer: {elementId: element_id, height: 600, width: 2*canvases[canvas].width, backend: VF.Renderer.Backends.CANVAS}});
                        var easyscore = vf.EasyScore();

                        easyscore.set({ time: timesig_num + '/' + timesig_type });

                        var context = vf.context;

                        context.scale(2,2);

                        this.add_bar(canvases[canvas], vf, bar_width, timesig_num, timesig_type, easyscore, easyscore_notes[i][0], easyscore_notes[i][1]);

                        vf.draw();
                }

                this.reposition_notes(midi, easyscore_notes, clocktime);

                this.retrieve_notes();

                this.last_tickindex = this.xposs.length - 1;

                // scale up the width according to the scale factor

                for (var i=0;i<canvases.length;i++) {
                        canvases[i].width *= 2;
                }

                console.log(this.xposs);
                
                // console.log(canvases);

                return this.dump();
        }
}



